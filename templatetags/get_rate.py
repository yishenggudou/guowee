# -*- coding:utf-8 -*-
from django import template  
register = template.Library() 

@register.filter('get_rate_data')
def get_rate_data(obj,epk=1):
    if epk == 1:
        return obj.get_good_rate()
    elif epk == -1:
        return obj.get_bad_rate()
    else:
        return u'暂无'
    
@register.filter('get_obj')
def get_obj(obj):
    return obj.all()

@register.filter('order_data')
def order_data(obj):
    return obj.all().order_by('-post_time')[:3]


@register.filter('order_method')
def get_method(obj):
    pass

@register.filter('showdel')
def showdel(obj,author_id):
    a=obj.show_del(int(author_id))
    return a

@register.filter('gethateclass')
def gethateclass(obj,author_id):
    a=obj.filter(author=int(author_id))
    if a.exists():
            return 'good_black'
        else:
            return 'good'
@register.filter('getlikeclass')
def getlikeclass(obj,author_id):
    a=obj.filter(author=int(author_id))
    if a.exists():
            return 'bad_red'
        else:
            return 'bad'
        
@register.filter('unescape')
def unescape(s):
    s = s.replace("&lt;", "<")
    s = s.replace("&gt;", ">")
    # this has to be last:
    s = s.replace("&amp;", "&")
    return s  