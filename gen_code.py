#! /usr/bin/python
# -*- coding: utf-8 -*-

import hashlib
import random

S1 = 'abcdefghijklmnopqrstuvwxyz'
S2 = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'
S3 = '0123456789~!@#$%&*)(+|\=/?><'
S = S1 + S2 + S3

CHAR_LEN = 5 #几个字符作为验证码基本段
SECRET_KEY = 'ec&=)7!ppqe+*!(s_v!6mvw@@5^yi)wris=8^j&(h6)k2lysic'

token = ''
for i in range(CHAR_LEN):
    token += random.choice(S)

code = token + hashlib.md5(token + SECRET_KEY).hexdigest()[:4]
print code
