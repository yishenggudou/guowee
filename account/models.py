#! /usr/bin/python
# -*- coding: utf-8 -*-

import os

from django.contrib.auth.models import User
from django.db import models
from django.conf import settings


class UserProfile(models.Model):

    id = models.AutoField('ID', primary_key=True)
    user = models.ForeignKey(User, verbose_name='关联用户', null=False, unique=True)
    nick_name = models.CharField('昵称', max_length=30, db_index=True)
    region = models.CharField('地区', null=True, max_length=100)
    register_datetime = models.DateTimeField('注册时间', auto_now_add=True)
    sex = models.BooleanField('性别', default=True)
    bio = models.TextField('简介', blank=True)

    @property
    def avatars(self):
        dirname_big = '%s/%s/%s/' % ('avatars', 'b', self.user.id % 100)
        dirname_small = '%s/%s/%s/' % ('avatars', 's', self.user.id % 100)
        dirname_huge = '%s/%s/%s/' % ('avatars', 'h', self.user.id % 100)
        filename_big = os.path.join(dirname_big, '%s.jpg' % self.user.id)
        filename_small = os.path.join(dirname_small, '%s.jpg' % self.user.id)
        filename_huge = os.path.join(dirname_huge, '%s.jpg' % self.user.id)
        if os.path.isfile(os.path.join(settings.MEDIA_ROOT, filename_big)):
            return filename_big, filename_small, filename_huge
        else:
            return 'avatars/default/avatar-big.png', 'avatars/default/avatar.png', ''

    class Meta:
        db_table = 'account_profile'
