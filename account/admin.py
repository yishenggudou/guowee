#! /usr/bin/python
# -*- coding: utf-8 -*-

from django.contrib import admin
from models import UserProfile

class UserProfileAdmin(admin.ModelAdmin):
    list_display = ('id', 'user', 'nick_name', 'region', 'sex',
                    'register_datetime',)
    fields = ('nick_name', 'region')

admin.site.register(UserProfile, UserProfileAdmin)
