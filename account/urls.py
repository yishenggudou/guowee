# -*- coding:utf-8 -*-

from django.conf.urls.defaults import *
from django.contrib import admin

admin.autodiscover()

from views import (login,
                   logout,
                   register,
                   send_mail,
                   activate,
                   test,
                   reset_password,
                   set_info,
                   find_password,
                   upload,
                   save_avatars,
                   avatars,
                  )


urlpatterns = patterns('',
     (r'^test/{0,1}$', test),
     (r'^login/{0,1}$', login),
     (r'^logout/{0,1}$', logout),
     (r'^register/{0,1}$', register),
     (r'^actmail/(?P<user_id>\d+)/{0,1}$', send_mail),
     (r'^activate/(?P<user_id>.*?)/(?P<code>.*)/{0,1}$', activate),
     (r'^reset/{0,1}$', reset_password),
     (r'^find-password/{0,1}$', find_password),
     (r'^upload/{0,1}$', upload),
     (r'^save_avatars/{0,1}$', save_avatars),
     (r'^avatars/{0,1}$', avatars),
     (r'^$', set_info),
)
