// JavaScript Document
//input输入框插件
var posturl='';
var itemid='';
$(document).ready(function () {
    $("#posterror").hide();
    //$("#tip_layout").hide();
    //$(".add_app").hide();
    jQuery.fn.myTest = function (options) {
        //方法  文本框默认字 设置参数对象  默认值，颜色，高度，
        var defaults = {
            obj_opts: "",
            obj_tx: "",
            //调用方法时程序可设置传入默认值
            obj_color: "",
            //设置颜色
            obj_dcolor: "",
            //默认颜色
            obj_h: "",
            //设置高度
            obj_dh: "" //默认高度
        }
        var opts = $.extend(defaults, options);
        return this.each(function () {
            var _this = $(this).find(opts.obj_opts);
            _this.val(opts.obj_tx); //传入默认值
            _this.focus(function () { //获取焦点
            	$(".add_app").show();
                if (_this.val() === opts.obj_tx) { //如果文本框内容文字等于默认值
                    _this.val("").css({
                        "color": opts.obj_color
                    })
                    if (opts.obj_h != "") {
                        _this.height(opts.obj_h);
                    }
                };
            });
            _this.blur(function () { //失去焦点
                if (_this.val() === "") { //如果文本框内容文字等于null
                    _this.val(opts.obj_tx).css({
                        "color": opts.obj_dcolor
                    })
                    if (opts.obj_h != "") {
                        _this.height(opts.obj_dh);
                    }
                }
            });
        });
    };
    $(".search-box").myTest({ //搜索输入框
        obj_opts: ".input-tx",
        obj_tx: "点评一下吧",
        obj_color: "#4d4d4d",
        obj_dcolor: "#A8A1A6"
    });
	$(".applink-box").myTest({ //搜索输入框
        obj_opts: ".app-link",
        obj_tx: "粘贴app链接到这里",
        obj_color: "#4d4d4d",
        obj_dcolor: "#A8A1A6"
    });


    //loading...
    $(".shareSubmit").click(function () {
        //出现加载loading图片
        $(".shareSubmit").fadeOut("slow");
        $(".loadimg").fadeIn("slow");
        //ajaxthe  post
        $(function () {

            checkid();
            var st = $("#JqPostForm").serialize();
            var postcont=st+posturl+'&id='+itemid;
            var patt1 = new RegExp("itunes.apple.com");
				//var patt2  = new RegExp("http://itunes.apple.com.{0,}/id\\d{0,}");
				//var te=st.match(patt2)[0];
				//var tt=st.replace(te,'');
            //if (patt1.test(st) == false) {
             //   var errortext = '你发布的不是app链接，无法发布';
             //   $("#posterror").eq(0).html(errortext).slideDown(400);
             //   $("#posterror").eq(0).html(errortext).slideUp(8000);
           // } else 
            if (st.length < 10) {
                //window.open("http://www.baidu.com");
                var errortext = '你写得太少了，说点什么吧！';
                $("#posterror").eq(0).html(errortext).slideDown(400);setTimeout(function(){$("#posterror").eq(0).html(errortext).slideUp(1000);},4000)
                //window.open("http://www.baidu.com")
            } else {
                $.ajax({
                    type: 'POST',
                    url: "/posturl/",
                    data: postcont,
                    dataType: "html",
                    success: function (data) {
                        if (data == 'False') {
                            var errortext = '两天内已经有人分享过该app了，请注意查看哦';
                            $("#posterror").eq(0).html(errortext).slideDown(400);setTimeout(function(){$("#posterror").eq(0).html(errortext).slideUp(1000);},4000)
                        } else if (data == 'notapp') {
                            var errortext = '你发布的不是app链接，无法发布';
                            $("#posterror").eq(0).html(errortext).slideDown(400);setTimeout(function(){$("#posterror").eq(0).html(errortext).slideUp(1000);},4000)
                        } else if (data == 'cantfind') {
                            var errortext = '找不到你分享的app，请确认后再发布';
                            $("#posterror").eq(0).html(errortext).slideDown(400);setTimeout(function(){$("#posterror").eq(0).html(errortext).slideUp(1000);},4000)
                        } else {
                            $("div.app_cont").prepend(data);
                            $("#JqPostForm>p>input:first").attr('value', '继续分享');
                            //window.location.reload();
                        }
                    },
                    error: function () {
                        var errortext = '两天内已经有人分享过该app了，请注意查看哦';
                        $("#posterror").eq(0).html(errortext).slideDown(3000);setTimeout(function(){$('#posterror').eq(0).html(errortext).slideUp(1000);}, 4000)

                    },
                });
            }
        });
        //加载loading图片消失
        $(".loadimg").fadeOut("slow");
        $(".shareSubmit").fadeIn("slow");
		  $("#input2").val("点评一下吧")

    });
	//发布链接
    $(".tipLink").click(function () {
       $(".linkForm").show();
	   if($("#input2").val()=="点评一下吧"){
			$("#input2").val("");
		}

    });
    $(".close").click(function () {
        $(".linkForm").hide();
		if($("#input1").val()==""&&$("#input2").val()==""){
			$("#input2").val("点评一下吧");
		}
		else
		{
			//$("#input2").val();
			}
    })
	//取链接值
	$(".appname_box").hide();
	$(".L_btn").click(function () {     
	  var linkVal = $("#input1").val();
	  var textVal = $("#input2").val();
	  var patt1 = new RegExp("itunes.apple.com");
	  var patt2 = new RegExp("itunes.apple.com");
	  if(linkVal==""){
		  $(".add_app p").hide();
		   $("#tip").html("链接为空，请输入" + "<a class='re_send'>重新输入</a>");
		   $("#tip a").click(function(){
				$("#tip").hide();	
				$(".add_app p").show();
				$("#input1").val("粘贴app链接到这里");
			});
			 return false
		  }
		  else if (patt1.test(linkVal) == false){
		  $(".add_app p").hide();
		  $("#tip").html("你发布的不是app链接，请输入正确的app链接" + "<a class='re_send'>重新输入</a>");
		  $("#tip a").click(function(){
				$("#tip").hide();	
				$(".add_app p").show();
				$("#input1").val("粘贴app链接到这里");
			})
		  }
		  else{
		  		posturl=linkVal;
		  		$.ajax({
                    type: 'GET',
                    url: "/posturl/",
                    data: {'app':posturl},
                    dataType: "json",
                    success: function (data){
                    if (data == 'cantfindappname'){
					$(".add_app p").hide();
                    $("#tip").html("该app链接不合法，请输入正确的app链接" + "<a class='re_send'>重新输入</a>");
					$("#tip a").click(function(){
						$("#tip").hide();	
						$(".add_app p").show();
						$("#input1").val("粘贴app链接到这里");
					})
                   // $(".linkForm").show();
							}
						  else{
                    var a=$('.appname_box');
                    //alert(data);
                    //da=jQuery.parseJSON(data);
                    a.find('span').eq(0).text(data.trackName);
                    a.find('span').eq(1).text(data.price);
                    itemid=data.id;
                    //a.find('span').eq(0).text(da);
					$(".appname_box").show();
					$(".L_btn").parent("p").hide();
                    }
                    }
                    });
			  //$("#input2").val("#分享#" + $("#appname").text() + "(" + "设备" + ")" + textVal);
			  
			  
		 };
    });
	
	$(".close_T").click(function(){
		$(".appname_box").hide();
		$(".add_app p").show();
		$("#input1").val("粘贴app链接到这里");

	});
});

function checkid() {
    var cs = $("#posterror").attr("class");
    if (cs == "posterror") {} else {
        $("#posterror").attr('class', 'posterror');
    }
};
//评论 点击   
$(function () {
    $("table.digg tr td a").click(function () {
        if ($(this).attr('class') == 'bad' || $(this).attr('class') == 'bad_red') {
            var a = $(this);
            var posturl = a.attr('value');
            var lk="喜欢(";
            var end=")";
				var ulk="不喜欢(";
            $.ajax({
                type: 'POST',
                url: posturl,
                cache: false,
                success: function (data) {
                    var da = jQuery.parseJSON(data);
                    a.html(da.good);
                    var b = a.parents('td').parents('tr').next().children().eq(3).children();
                    b.html(lk+da.bad+end).show();
                    b.removeClass();
                    b.addClass("good");
                }
            });
            $(this).removeClass();
            $(this).addClass("bad_red");
            return false;
        } else if ($(this).attr('class') == 'good' || $(this).attr('class') == 'good_black') {

            var a = $(this);
            var posturl = a.attr("value");
            $.ajax({
                type: "POST",
                url: posturl,
                cache: false,
                success: function (data) {
                    var da = jQuery.parseJSON(data);
                    a.html(ulk+da.bad+end);
                    var b = a.parents("td").parents("tr").prev().children().eq(2).children();
                    b.html(da.good).show();
                    b.removeClass();
                    b.addClass('bad');
                }
            });
            $(this).removeClass();
            $(this).addClass("good_black");
            return false;
        } else {
            var a = $(this);
            var posturl = a.attr("value");
        }
    })
});

function rate(me) {
	var lk="喜欢(";
   var end=")";
	var ulk="不喜欢(";
    if ($(me).attr('class') == 'bad' || $(me).attr('class') == 'bad_red') {
        var a = $(me);
        var posturl = a.attr('value');
		  
        $.ajax({
            type: 'POST',
            url: posturl,
            cache: false,
            success: function (data) {
                var da = jQuery.parseJSON(data);
                a.html(lk+da.good+end);
                var b = a.parents('td').parents('tr').next().children().eq(3).children();
                b.html(ulk+da.bad+end).show();
                b.removeClass();
                b.addClass("good");
            }
        });
        $(me).removeClass();
        $(me).addClass("bad_red");
        return false;
    } else if ($(me).attr('class') == 'good' || $(me).attr('class') == 'good_black') {

        var a = $(me);
        var posturl = a.attr("value");
        $.ajax({
            type: "POST",
            url: posturl,
            cache: false,
            success: function (data) {
                var da = jQuery.parseJSON(data);
                a.html(ulk+da.bad+end);
                var b = a.parents("td").parents("tr").prev().children().eq(2).children();
                b.html(lk+da.good+end).show();
                b.removeClass();
                b.addClass('bad');
            }
        });
        $(me).removeClass();
        $(me).addClass("good_black");
        return false;
    } else {
        var a = $(me);
        var posturl = a.attr("value");
    }

}

