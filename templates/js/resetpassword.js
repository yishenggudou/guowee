// JavaScript Document
$(document).ready(function(){
	$.formValidator.initConfig({onError:function(msg){alert(msg)}});
	
	$("#pwd3").formValidator({onshow:"",onfocus:"请输入目前密码",oncorrect:""}).InputValidator({min:5,onerror:"格式不对,请确认"});
	
	$("#pwd1").formValidator({onshow:"",onfocus:"请输入密码",oncorrect:""}).InputValidator({min:5,onerror:"密码至少5个字符,请确认"});
	
	$("#pwd2").formValidator({onshow:"",onfocus:"请确认密码，两次密码必须一致",oncorrect:""}).InputValidator({min:5,onerror:"重复密码不能为空,请确认"}).CompareValidator({desID:"pwd1",operateor:"=",onerror:"两次密码不一致,请确认"});
	
});


function viewnone(e){
	e.style.display=(e.style.display=="none")?"":"none";
}
