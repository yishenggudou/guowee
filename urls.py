# -*- coding:utf-8 -*-
from django.conf.urls.defaults import *
from django.contrib import admin
admin.autodiscover()
import settings
from django.views.decorators.cache import cache_page
from django.views.static import serve as djangostatic
from appstore.views import view_rebang
#import django_cron
#django_cron.autodiscover()
urlpatterns = patterns('',
     (r'^admin/doc/', include('django.contrib.admindocs.urls')),
     (r'^admin/', include(admin.site.urls)),
     (r'^account$','account.views.set_info'),
     (r'^account/', include('account.urls')),
     (r'^invitation/', include('invitation.urls')),
     (r'^sitemap.xml$',include('appstore.sitemap')),
     #(r'^comment/',include('comments.urls')),
     (r'^$', 'appstore.views.view_index'),
     (r'^register/{0,1}$', 'account.views.register'),
     (r'^login/{0,1}$', 'account.views.login'),
     (r'^posturl/$','appstore.views.view_post_appurl'),
     (r'^top/{0,1}$','appstore.views.view_rebang'),
     #(r'^top/$',cache_page(view_rebang,60)),
     (r'^None/{0,1}$','appstore.views.view_redirect'),
     (r'^updatedb/{0,1}$','appstore.views.updatedb'),
     (r'^new/{0,1}','appstore.views.view_new_app'),
     (r'^app/(?P<item_id>\d{0,})/{0,1}$','appstore.views.view_item_detail'),
     (r'^app/(?P<item_id>\d{0,})/(?P<preference>.*?)/{0,1}$','appstore.views.view_app_like'),
     (r'^(?P<author_id>\d{0,})/(?P<preference>.*?)/applist/{0,1}$','appstore.views.view_user_likeapp'),
     (r'^user/\S{0,30}','appstore.views.view_user_home'),
     (r'^post/(?P<post_id>\d{0,})/{0,1}$','appstore.views.view_app_detail'),
     (r'^cron/{0,1}$','appstore.views.view_cron'),
     (r'^myshare/{0,1}','appstore.views.view_myshare'),
     (r'^(?P<author_id>\d{0,})/{0,1}$','appstore.views.view_myshare'),
     (r'^rate/(?P<flag>\S{0,4})/(?P<item_id>\d*?)/{0,1}$','appstore.views.view_post_rating_new'),
     (r'^check/(?P<flag>\S{0,4})/(?P<item_id>\d*?)/(?P<author_id>.*?)/{0,1}$','appstore.views.view_checklike'),
     (r'^del/comment/(?P<post_id>\d*?)/(?P<comment_id>\d*?)/(?P<page>\d{0,})','appstore.views.view_delete_comment'),
     #(r'^site_media/(?P<path>.*)$', cache_page(djangostatic,60*60*2),{'document_root': settings.STATIC_PATH},),
     (r'^site_media/(?P<path>.*)/{0,1}$', 'django.views.static.serve',{'document_root': settings.STATIC_PATH},),
     (r'^media/(?P<path>.*)/{0,1}$', 'django.views.static.serve', {'document_root': '/usr/local/lib/python2.6/dist-packages/django/contrib/admin/media/'}),
     (r'^static/(?P<path>.*)/{0,1}$', 'django.views.static.serve', {'document_root':settings.MEDIA_ROOT}),
)
