# -*- coding:utf-8 -*-
# Django settings for candou project.
import os
base_dir=os.getcwd()
DEBUG = True
TEMPLATE_DEBUG = DEBUG

ADMINS = (
    # ('Your Name', 'your_email@domain.com'),
)

MANAGERS = ADMINS
DATABASE_ENGINE = 'sqlite3'           # 'postgresql_psycopg2', 'postgresql', 'mysql', 'sqlite3' or 'oracle'.
DATABASE_NAME = os.path.join(base_dir,'gw.db')             # Or path to database file if using sqlite3.
DATABASE_USER = ''             # Not used with sqlite3.
DATABASE_PASSWORD = ''         # Not used with sqlite3.
DATABASE_HOST = ''             # Set to empty string for localhost. Not used with sqlite3.
DATABASE_PORT = ''            # Set to empty string for default. Not used with sqlite3.

# Local time zone for this installation. Choices can be found here:
# http://en.wikipedia.org/wiki/List_of_tz_zones_by_name
# although not all choices may be available on all operating systems.
# If running in a Windows environment this must be set to the same as your
# system time zone.
TIME_ZONE = 'Asia/Chongqing'

# Language code for this installation. All choices can be found here:
# http://www.i18nguy.com/unicode/language-identifiers.html
LANGUAGE_CODE = 'en-us'

SITE_ID = 1

# If you set this to False, Django will make some optimizations so as not
# to load the internationalization machinery.
USE_I18N = True

# Absolute path to the directory that holds media.
# Example: "/home/media/media.lawrence.com/"
#MEDIA_ROOT = '/home/zhb/eclipseworkplace/candou/src/candou/candous/media'
MEDIA_ROOT = os.path.join(base_dir,'media')
# URL that handles the media served from MEDIA_ROOT. Make sure to use a
# trailing slash if there is a path component (optional in other cases).
# Examples: "http://media.lawrence.com", "http://example.com/media/"
MEDIA_URL = '/static/'

# URL prefix for admin media -- CSS, JavaScript and images. Make sure to use a
# trailing slash.
# Examples: "http://foo.com/media/", "/media/".
ADMIN_MEDIA_PREFIX = '/media/'

# Make this unique, and don't share it with anybody.
SECRET_KEY = 'ec&=)7!ppqe+*!(s_v!6mvw@@5^yi)wris=8^j&(h6)k2lysic'

# List of callables that know how to import templates from various sources.
TEMPLATE_LOADERS = (
    'django.template.loaders.filesystem.load_template_source',
    'django.template.loaders.app_directories.load_template_source',
    'django.template.loaders.eggs.load_template_source',
)

MIDDLEWARE_CLASSES = (
    
    'django.middleware.common.CommonMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    
)

ROOT_URLCONF = 'urls'

TEMPLATE_DIRS = (
                 os.path.join(base_dir,'templates'),
    
)


TEMPLATE_TAGS = (
    os.path.join(base_dir,'templatetags'),
    'templatetags',
)

INSTALLED_APPS = (
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.sites',
    'django.contrib.sitemaps',
    'appstore',
    'account',
    'invitation',
)

AUTH_PROFILE_MODULE = 'account.UserProfile'
LOGIN_URL='/account/login'
LOGOUT_URL='/account/logout'
#APPEND_SLASH=True

#cache setting
CACHE_BACKEND = 'memcached://127.0.0.1:11211/'
CACHE_MIDDLEWARE_SECONDS=60
CACHE_MIDDLEWARE_KEY_PREFIX='guowee'
CACHE_MIDDLEWARE_ANONYMOUS_ONLY=True

UPLOAD_ROOT=os.path.join(base_dir,'appstore','media')
STATIC_PATH=os.path.join(base_dir,'templates')

EMAIL_USE_TLS = True
EMAIL_HOST = 'smtp.gmail.com'
EMAIL_HOST_USER = 'no-reply@guowee.com'
EMAIL_HOST_PASSWORD = 'xiguapi2011'
EMAIL_PORT = 587
DEFAULT_FROM_EMAIL='果味网 <no-reply@guowee.com>'
EMAIL_CONTEXT='''主题: 请激活你的帐号，完成注册
 

亲爱的tongxiaobai：

欢迎加入西瓜(xigua.me)!

请点击下面的链接完成注册：

http://xigua.me/accounts/auth/%s

如果以上链接无法点击，请将上面的地址复制到你的浏览器(如IE)的地址栏进入西瓜。


--西瓜网

(这是一封自动产生的email，请勿回复。)'''

