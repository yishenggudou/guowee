# -*- coding:utf-8 -*-


class cut_page:
    def __init__(self,li_page,member=70,**args):
        '''li_page is a list of the queryset for the member'''
        if args.get('page') !=None:
            self.page=args.get('page')
        else:
            self.page=1
        if args.get('start') !=None:
            self.start=args.get('start')
        else:
            self.start=0
        print self.start
        print args
        self.li_page=li_page
        self.num_member=len(self.li_page)
        self.member=member
        self.page_show={
                        'prev_page':'',
                        'prev_group':'',
                        'next_page':'',
                        'next_group':'',
                        'num_page':'',
                        'currect':'',
                        'firstpage':'',
                        'secondpage':'',
                        'penultimate':'',
                        'lastpage':'',
                        'bodylist':[],
                        'pagecount':'',
                        'startellipsis':False,
                        'endellipsis':False,
                        'startelpage':'',
                        'endelpage':'',
                    }
    def page2matrix(self):
        self.rows=self.member
        col=divmod(self.num_member,self.member)
        if col[1]:
            if col[0] == 0:
                self.cols=1
            else:
                self.cols=col[0]+1
        elif col[1]==0:
            self.cols=col[0]+1
        self.matrix=[]
        for i in range(self.cols):
            row=self.li_page[int(i*self.rows) : int(i*self.rows+self.rows)]
            self.matrix.append(row)
        return self.matrix
    def cut_page(self):
        self.page2matrix()
        if self.page:
             pass
        elif self.start:
            a=divmod(self.start,self,member)
            if a[1]:
                if a[0] == 0:
                    self.page=1
                    self.page_show['pagecount']=1
                else:
                    self.page=a[0]+1
            elif a[1]==0:
                self.page=a[0]+1
        self.page=int(self.page)
        self.page_show['currect']=self.page
        self.page_show['pagecount']=self.cols
        if self.cols ==1:
            self.page_show['bodylist']=''
        elif self.cols<=10:
            self.page_show['bodylist']=range(1,int(self.cols)+1)
        elif self.cols>10 and self.page<=6:
            self.page_show['bodylist']=range(1,7)+range(7,self.page+5)+[self.cols-1,self.cols]
            self.page_show['endellipsis']=True
        elif self.cols>10 and self.page>6 and  self.page <self.cols-4:
            self.page_show['bodylist']=range(1,3)+range(self.page-4,self.page+5)+[self.cols-1,self.cols]
            self.page_show['startellipsis']=True
            self.page_show['endellipsis']=True  
        elif self.cols>10 and self.page>6 and  self.page >=self.cols-4:
            self.page_show['bodylist']=range(1,3)+range(self.page-4,self.cols+1)
            self.page_show['startellipsis']=True
        if self.page_show['startellipsis']==True:
            self.page_show['startelpage']=2
        if self.page_show['endellipsis']==True :
            self.page_show['endelpage']=self.page_show['bodylist'][-3] 
        return self.page_show
    
    @property   
    def li_slice(self):
        print self.page
        start=(int(self.page)-1)*self.member
        end=start+self.member
        return (start,end)
    
    
    
from django.core.cache import cache
from appstore.models import  item,  Comments, Rating, waring, post
def cache_top_page(length=50,timeout=60):
    try:
        li_post=cache.get('li_post').split('+')
        data=[]
        for i in li_post:
            data.append(post.objects.get(id=int(i)))  
            pass          
    except:
        li_post=post().getrebang()[:length]
        #val=reduce(lambda x,y:''.join([x,'+',y]), li_page)
        val=''
        for i in li_post:
            val=''.join([val,'+',str(i.id)])
        cache.set('li_post', val, timeout)
        data=li_post
    return data
        
    
def cache_val(target):
    def wrapper(*args, **kwargs):
        kwargs.update({'debug': True}) # Edit the keyword arguments -- here, enable debug mode no matter what
        print 'Calling function "%s" with arguments %s and keyword arguments %s' % (target.__name__, args, kwargs)
        return target(*args, **kwargs)

    wrapper.attribute = 1
    return wrapper
           

