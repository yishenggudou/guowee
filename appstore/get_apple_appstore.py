# -*- coding:utf-8 -*- 
try:
    from BeautifulSoupa import BeautifulSoup
except:
    from BeautifulSoup import BeautifulSoup
import re
import chardet
import urllib2
import time
import datetime

def device_check(s,f):
    '''s 为如果app图标下面有：This app is designed for both iPhone and iPad f为Requirements: Compatible with iPhone, iPod touch, and iPad. Requires iOS 3.2 or later'''
    if 'iPad' in str(s) and 'iPhone' in str(s):
        return 'both'
    elif 'iPhone' in str(f):
        return 'iPhone'
    elif not('iPhone' in str(f)) and 'iPad' in str(f):
        return 'iPad'
    elif 'Mac' in str(f):
        return 'Mac'
    else:
        return 'None'
def unescape(s):
    
    s = s.replace("&lt;", "<")
    s = s.replace("&gt;", ">")
    # this has to be last:
    s = s.replace("&amp;", "&")
    s = s.replace("&nbsp;",' ')
    #s=re.sub("&[\s\S]*;",'',s)
    return s    

class findinfo():
    def __init__(self,url):
        self.info={}
        self.url_app_itunes=url
        a=re.findall('com/([\S]*?)app[\S]*?/id(\d{0,})',str(self.url_app_itunes))
        self.info['app_id']=int(a[0][1])
        self.info['Region']=self.do_encode(a[0][0].split('/')[0] or 'us')
        self.body=urllib2.urlopen(self.url_app_itunes).read()
        self.html=self.do_encode(self.body)
        self.info['name_app']=self.do_encode(re.findall('<h1>([\s\S]*?)</h1>',self.html)[0])
    
    def do_encode(self,tx):
        '''encode the html code to right style'''
        try:
            message_body=tx.encode('utf-8')
        except:
            nb_encode=chardet.detect(tx) 
            nb_code=nb_encode['encoding']
            #logging.info(nb_code)
            message_body=tx.decode(nb_code,'ignore').encode('utf-8')
        return message_body
    
    
    def get_html(self):
        '''get the html code from the web url and anlysis the html code'''
        self.goodhtml=re.findall('''(<div rating-software="\d00,itunes-games" parental-rating="1" class="lockup product application">[\s\S]*?</div>[\s\S]*?</div>[\S\s]*?</div>[\s\S]*?</div>)''',self.html)[0]
    @property
    def get_appname(self):
        return self.do_encode(re.findall('<h1>([\s\S]*?)</h1>',self.html)[0])
    def find_info(self):
        '''find the right data and add it to dict'''
        #self.info={}
        soup1=BeautifulSoup(self.html)
        self.info['price']=soup1.find('div',{'class':'price'}).string
        self.info['url_app']=soup1.find('div',{'class':'lockup product application'}).find('a').attrs[0][1]
        self.info['url_img_app']=soup1.find('div',{'class':'lockup product application'}).find('img').attrs[0][1]
        try:
            self.info['language']=soup1.find('li',{'class':'language'}).find('span').nextSibling
        except:
            self.info['language']=''
        try:
            self.info['genre']=soup1.find('li',{'class':'genre'}).find('a').string
        except:
            self.info['genre']=''
        try:
            self.info['url_genre']=soup1.find('li',{'class':'genre'}).find('a').attrs[0][1]
        except:
            self.info['url_genre']=''
        #self.info['release_date']=soup1.find('div',{'class':'lockup product application'}).findAll('li')[2].find('span').nextSibling
        
        try:
            self.info['vesion_app']=soup1.find('div',{'class':'lockup product application'}).findAll('li')[3].find('span').nextSibling
        except:
            self.info['vesion_app']=''
        try:
            self.info['app_requirements']=soup1.find('div',{'class':'lockup product application'}).find('span',{'class':'app-requirements'}).nextSibling
        except:
            self.info['app_requirements']=''
        try:
            self.info['developer']=soup1.find('div',{'class':'lockup product application'}).findAll('li')[7].find('span').nextSibling
        except:
            self.info['developer']=''
        try:
            self.info['description']=unescape(str(soup1.find('div',{'class':'product-review'}).find('p')))
        except:
            self.info['description']=''
        try:
            self.info['screenshots_iphone']=''.join([i.attrs[0][1] for i in soup1.find('div',{'class':"content iphone-screen-shots"}).findAll('img')])
        except:
            self.info['screenshots_iphone']=''
        try:
            self.info['screenshots_ipad']=''.join([i.attrs[0][1] for i in soup1.find('div',{'class':"content ipad-screen-shots"}).findAll('img')])
        except:
            self.info['screenshots_ipad']=''
        try:
            iphone=[i.attrs[0][1] for i in soup1.find('div',{'class':"content iphone-screen-shots"}).findAll('img')]
            ipad=[i.attrs[0][1] for i in soup1.find('div',{'class':"content ipad-screen-shots"}).findAll('img')]
            mac=[i.find('img').attrs[0][1] for i in soup1.findAll('div',{'class':"lockup"})]
            new=list(set(mac)-set(iphone)-set(ipad))
            self.info['screenshots_mac']=''.join(new)
        except:
            self.info['screenshots_mac']=''
        try:
            d=re.findall ('(\d{4})\S*?(\d{2})\S*?(\d{2})',str(soup1.find('li',{'class':'release-date'})))
            self.info['release_date']=datetime.date(int(d[0][0]),int(d[0][1]),int(d[0][2]))
        except:
            try:
                d=str(soup1.find('li',{'class':'release-date'}))
                b=re.findall('</span>([\s\S]*?</li>)',d)[0]
                self.info['release_date']=datetime.date.strptime(b, "%b %d, %Y").date()
            except:
                self.info['release_date']=datetime.datetime.now().date()
        if soup1.find('div',{'class':'fat-binary-blurb'}) is not(None):
            self.info['size']=soup1.find('div',{'class':'lockup product application'}).findAll('span')[7].nextSibling
        else:
            self.info['size']=soup1.find('div',{'class':'lockup product application'}).findAll('span')[5].nextSibling
        try:
            d=soup1.find('div',{'class':'fat-binary-blurb'}) 
            self.info['device']= device_check(str(d),str(self.info['app_requirements']))
        except:
            self.info['device']=device_check('',str(self.info['app_requirements']))
    @property     
    def get_data(self):
        self.get_html()
        self.find_info()
        '''return the dict for Use'''
        return self.info


def do_encode(tx):
        '''encode the html code to right style'''
        try:
            message_body=tx.encode('utf-8')
        except:
            nb_encode=chardet.detect(tx) 
            nb_code=nb_encode['encoding']
            #logging.info(nb_code)
            message_body=tx.decode(nb_code,'ignore').encode('utf-8')
        return message_body
    
class find_appupdata():
    def __init__(self,**args):
        '''the term is the key words of the app's name'''
        self.base_url=r'http://ax.itunes.apple.com/WebObjects/MZStoreServices.woa/wa/wsLookup?id=366513082'
        self.query={'term':'',
                    'entity':'software',
                    'limit':'200',
                    'explicit':'Yes',}
        self.query.update(args)
        
    def updata(self):
        import json,urllib
        self.url_query=''.join([self.base_url,urllib.urlencode(self.query)])
        self.js_data=urllib2.urlopen(self.url_query).read()
        self.anal_data=json.loads(self.js_data)
        self.right_data=self.anal_data['results']
        return self.right_data
        
        
    def save2database(self):
        try:
            import  item
        except:
            from guowee.appstore.models import  item
        db=item.objects.all().filter(app_id=self.appid).get()
        db.app_id=int(self.right_data.get('app_id',db.app_id))
        db.Region=do_encode(self.right_data.get('Region',db.Region))
        db.name=do_encode(self.right_data.get('trackName',db.name))
        db.app_url=do_encode(self.right_data.get('app_url',db.app_url))
        db.img_url=do_encode(self.right_data.get('img_url',db.img_url))
        db.price=do_encode(self.right_data.get('price',db.price).replace('0.0','Free'))
        db.size=do_encode(''.join([str(int(self.right_data.get('fileSizeBytes'))/1024),'MB']))
        db.genre=do_encode(self.right_data.get('primaryGenreName',db.genre))
        db.language=do_encode(self.right_data.get('languageCodesISO2A'.lower(),db.language))
        db.develpoer=do_encode(self.right_data.get('develpoer',db.develpoer))
        db.vesion_app=do_encode(self.right_data.get('vesion',db.vesion_app))
        db.save()
           
  
class updata_use_api():
    def __init__(self,id,args_api='getproductdetails',**args):
        '''should give a appituns url'''
        self.url_app=args.get('url') or ''.join(['http://itunes.apple.com/us/app/id',str(id),'?mt=8'])
        self.a=re.findall('com/([\S]*?)app[\S]*?/id([\S]*?)($|\?)',str(self.url_app))
        self.api_key='dESpRMknvortcDhz1gSAyl0Ii4hGyw68cd9e3c9f14fe3b'
        self.url='http://api.itunesapis.com/rest/getproductdetails/?a=dESpRMknvortcDhz1gSAyl0Ii4hGyw68cd9e3c9f14fe3b&productId=12345678'
        self.base_url='http://api.itunesapis.com/rest/'
        self.args_api=args_api
        self.product_id=id 
        self.query={'productId':self.product_id,
                    'a':self.api_key
                    }     
        self.query.update(args)
        self.info={}
        self.info['Region']=do_encode(self.a[0][0].split('/')[0] or 'us')
    def call_api(self):
        import urllib
        post_data=urllib.urlencode(self.query)
        self.call_url=''.join([self.base_url,self.args_api,'/?',post_data])
        self.data_callback=urllib2.urlopen(self.call_url).read()
        return self.data_callback
        
    def Analysis_data(self):
        self.call_api()
        from xml.dom import minidom
        data=minidom.parseString(self.data_callback)
        result=data.getElementsByTagName('result')[0]
        #self.app_vesion=result.getElementsByTagName('version')[0].firstChild.data
        return result
    @property   
    def get_data(self):
        s=time.time()
        result=self.Analysis_data()
        self.info['app_id']=result.getElementsByTagName('product')[0].getElementsByTagName('product-id')[0].childNodes[0].data
        try:
            self.info['Region']=result.getElementsByTagName('product')[0].getElementsByTagName('country')[0].childNodes[0].data 
        except:
            self.info['Region']=do_encode(self.a[0][0].split('/')[0] or 'us')
        self.info['name_app']=result.getElementsByTagName('product')[0].getElementsByTagName('product-name')[0].childNodes[0].data
        self.info['price']=result.getElementsByTagName('product')[0].getElementsByTagName('price')[0].childNodes[0].data
        self.info['url_app']=self.url_app
        self.info['url_img_app']=result.getElementsByTagName('product')[0].getElementsByTagName('iconurl')[0].childNodes[0].data
        try:
            self.info['language']=result.getElementsByTagName('product')[0].getElementsByTagName('language')[0].childNodes[0].data
        except:
            self.info['language']='us'
        try:
            self.info['genre']=result.getElementsByTagName('product')[0].getElementsByTagName('primary-category')[0].childNodes[0].data
            generid=result.getElementsByTagName('product')[0].getElementsByTagName('primary-categoryid')[0].childNodes[0].data
            self.info['url_genre']=''.join(['http://itunes.apple.com/',self.info['Region'],'/genre/id',generid,'?mt=8'])
        except:
            self.info['genre']=u'uncatalog'
            self.info['url_genre']=u''
        try:
            self.info['release_date']=result.getElementsByTagName('product')[0].getElementsByTagName('release-date')[0].childNodes[0].data
        except:
            self.info['release_date']=''
        try:
            self.info['vesion_app']=result.getElementsByTagName('version')[0].firstChild.data
        except:
            self.info['vesion_app']='1.0'
        self.info['developer']=result.getElementsByTagName('product')[0].getElementsByTagName('seller')[0].childNodes[0].data
        self.size=int(result.getElementsByTagName('product')[0].getElementsByTagName('filesize')[0].childNodes[0].data)
        self.size_mb=self.size/(1024.00*1024.00)
        self.size_str='%.2f' %(self.size_mb)
        self.info['size']=''.join([self.size_str,'MB'])
        print time.time()-s
        return self.info
    @property   
    def _get_alldata(self):
        s=time.time()
        import xml.etree.ElementTree as ElementTree
        import xmltodict as x2d
        result=self.call_api()
        tree = ElementTree.fromstring (result)
        xmldict = x2d.XmlDictConfig(tree)
        return xmldict
        
    @property   
    def _get_data(self):
        s=time.time()
        import xml.etree.ElementTree as ElementTree
        import xmltodict as x2d
        result=self.call_api()
        tree = ElementTree.fromstring (unescape(result))
        xmldict = x2d.XmlDictConfig(tree)
        data=xmldict['product']
        self.info['app_id']=data.get('product-id')
        self.info['Region']=data.get('country','us')
        if self.info['Region'] == None:
             self.info['Region']='us'
        self.info['name_app']=data.get('product-name')
        self.info['price']=data.get('price')
        self.info['url_app']=self.url_app
        self.info['url_img_app']=data.get('iconurl')
        self.info['language']=data.get('language','us')
        self.info['genre']=data.get('primary-category')
        generid=data.get('primary-categoryid')
        self.info['url_genre']=''.join(['http://itunes.apple.com/',str(self.info.get('Region')),'/genre/id',generid,'?mt=8'])
        self.info['release_date']=data.get('release-date','')
        self.info['vesion_app']=data.get('version')
        self.info['developer']=data.get('seller')
        self.info['description']=data.get('description')
        self.info['app_requirements']=data.get('app-requirements')
        try:
            self.info['release_date']=datetime.datetime.strptime(str(data.get('release-date')), "%Y-%m-%dT%H:%M:%Sz").date()
        except:
            pass
        try:
            self.info['screenshots_iphone']=''.join(data.get('screenshots').get('screenshot'))
        except:
            self.info['screenshots_iphone']=''
        self.size=data.get('filesize')
        self.size_mb=int(self.size)/(1024.00*1024.00)
        self.size_str='%.2f' %(self.size_mb)
        self.info['size']=''.join([self.size_str,'MB'])
        print time.time()-s
        return self.info 
    @property   
    def get_appname(self):
        return self._get_data['name_app']
      