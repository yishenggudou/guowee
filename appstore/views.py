# -*- coding:utf-8 -*-
from django.http import Http404, HttpResponse, HttpResponseRedirect,HttpResponseServerError 
from django.contrib.auth.models import User 
#from account.models import User
from django.contrib.auth.decorators import login_required
from django.http import HttpResponseRedirect,HttpRequest
from django.shortcuts import render_to_response
from appstore.models import  item,  Comments, Rating, waring, post
from get_apple_appstore import findinfo,do_encode,updata_use_api
from django.views.decorators.cache import cache_control,cache_page
from django.template import RequestContext
import datetime,json
#zouyesheng-----------------------------------------
#def login_required(func):
#    def f(request, *args, **kargs):
#        if request.ppid != -1:
#            return func(request, *args, **kargs)
#        else:
#            return HttpResponseRedirect('/account/login')
#    return f
#--------------------------------------------------

#class user_defult():
#    id=0
#    email=u''
#    nick_name=u'匿名用户'


#def user_ck(userid,*args):
#    if int(userid) == 0 or int(userid)==-1:
#        user=user_defult()
#        user.id=0
#        user.nick_name=u'匿名用户'
#    else:
#        if len(args)>0:
#            user=user_defult()
#            user.id=args[0].ppid
#            user.nick_name=args[0].nick_name
#            user.email=args[0].email
#        else:
#            user=User.objects.get(id=int(userid))
#    return user


#@cache_control(must_revalidate=True, max_age=3600)
@login_required
def view_home(request):
    #user=request.user
    user=request.user
    if request.method == 'GET':
        app=item.objects.order_by('-post_time')[:8]
        return render_to_response('topapp.html',{'user':user,
                                                    'appdata':app,
                                                    'index_page':'current',})   
        
    elif request.method == 'POST':
        pass
    
#@login_required
def view_post_appurl_older(request): 
    import re
    import urllib
    import cgi
    '''do some thing when user post a app's loaddown url'''
    user=request.user
    if user.is_authenticated():
        if request.method == 'GET':
            try:
                appname=findinfo(urllib.unquote(request.GET['app'])).get_appname
                return HttpResponse(cgi.escape(appname))
            except:
                return HttpResponse('cantfindappname')
        elif request.method == 'POST':
            s=request.POST['app_url']
            try:
                d_app_url=re.findall('(http://itunes.apple.com[\s\S]*?/id\d{0,})',s)[0]
            except:
                return HttpResponse('notapp')
            app_url=d_app_url+'?mt=8'
            post_content=s.replace(app_url,'')
            post_content=post_content.replace('&uo=4','')
            post_content=post_content.replace(d_app_url,'')
        
#----------------get app detail-------------------------------------------------
        #------------scan the app page and get detail-----------------------------
            #data=findinfo(app_url).get_data
            try:
                data=findinfo(app_url).get_data
            except:
                return HttpResponse('cantfind')
        
        #-------------use api call apple get detail-----------------------------
            #id=re.findall('id(\d{0,})',s)[0]
            #data=updata_use_api(id).get_data
             
#------------------------------end get app detail--------------------------------------
            a=item.objects.filter(app_id=data['app_id'])
            app_url=app_url.replace('http','itms')
        #app_url=''.join([app_url,'&uo=4'])
#-------------------------------------------------------------------------        
            b=a.filter(Region=data['Region'])  
            time_Distance=datetime.datetime.now()-datetime.timedelta(0,48)
            if not(a.exists() and b.exists()):
            #某个app可以被分享 ：1此appid不存在 2：此appid存在但是地区不一样
                import urllib2,os
                import settings
                #boob_image=urllib2.urlopen(data['url_img_app']).read()
                #file_img=open(os.path.join(settings.base_dir,'media',do_encode(data['name_app'])+'.jpg'),'wb')
                #file_img.write(boob_image)
                #file_img.close()
                post_db=post()
                app_db=item()
                app_db.app_url=do_encode(app_url)
                app_db.app_id=data['app_id']
                app_db.name=do_encode(data['name_app'])
                app_db.Region=data['Region']
                app_db.size=data['size']
                app_db.price=data['price']
                app_db.language=data['language']
                app_db.vesion_app=data['vesion_app']
                app_db.img_url=data['url_img_app']
                app_db.image_local_url=data['url_img_app']  #os.path.join('/static',do_encode(data['name_app'])+'.jpg')
                app_db.genre=data['genre']
                app_db.develpoer=data['developer']
                app_db.app_requirements=data['app_requirements']
                app_db.description=data['description']
                app_db.screenshots_iphone=data['screenshots_iphone']
                app_db.screenshots_mac=data['screenshots_mac']
                app_db.screenshots_ipad=data['screenshots_ipad']
                app_db.release_date=data['release_date']
                app_db.device=data['device']
                app_db.save()
                #print 'save item ok'
                post_db.item=app_db
                #print str(user.email)
                post_db.author=user
                #print str(user.email)
                post_db.content=do_encode(post_content)
                post_db.modify_time=datetime.datetime.now()
                #print '-----------------'
                post_db.save()
                #print 'save post ok '
                return  render_to_response('posturl_ok.html',{'post':post_db} ,context_instance=RequestContext(request))
            elif  a.exists() and not(b.exists())  :
                if a[0].post_time > time_Distance:
                    # a 存在 b不存在
                    post_db=post()
                    app_db=a
                    post_db.item=app_db
                    #print str(user.email)
                    post_db.author=user
                    #print str(user.email)
                    post_db.content=do_encode(post_content)
                    post_db.modify_time=datetime.datetime.now()
                    #print '-----------------'
                    post_db.save()
                    return  render_to_response('posturl_ok.html',{'post':post_db} ,context_instance=RequestContext(request))
                else:
                    return HttpResponse('False')
            elif  a.exists() and b.exists(): 
                if b[0].post_time > time_Distance : 
                    # a 存在 b不存在
                    post_db=post()
                    app_db=b
                    post_db.item=app_db
                    #print str(user.email)
                    post_db.author=user
                    #print str(user.email)
                    post_db.content=do_encode(post_content)
                    post_db.modify_time=datetime.datetime.now()
                    #print '-----------------'
                    post_db.save()
                    return  render_to_response('posturl_ok.html',{'post':post_db} ,context_instance=RequestContext(request))
                else:
                    return HttpResponse('False')
            else:  
                return HttpResponse('False')
    else:
        pass
    
    
def view_post_appurl(request): 
    import re
    import urllib
    import cgi
    from appleapi import *
    '''do some thing when user post a app's loaddown url'''
    user=request.user
    if user.is_authenticated():
        if request.method == 'GET':
            try:
                region=re.findall('//itunes.apple.com/([\s\S]*?)/app',urllib.unquote(request.GET['app']))[0]
                #region='cn'
            except:
                region='us'
            data=get_dict(re.findall('id(\d{0,})',urllib.unquote(request.GET['app']))[-1],region)
            app_db=save2item(data,region)
            return HttpResponse(json.dumps({'trackName':app_db.trackName,'price':app_db.price_format,'id':app_db.id}))
            #try:
            #    data=get_dict(re.findall('id(\d{0,})',urllib.unquote(request.GET['app']))[0],region)
            #    app_db=save2item(data,region)
            #    return HttpResponse(json.dumps({'trackName':app_db.trackName,'price':app_db.price_format,}))
            #except:
            #    return HttpResponse('cantfindappname')
        elif request.method == 'POST':
            s=request.POST['app_url']
            try:
                _d=re.findall('(itunes.apple.com[\s\S]*?/id\d{0,})',s)[0]
                d_app_url='http://'+_d
                d_item_url='itms://'+_d
            except:
                return HttpResponse('notapp')
            try:
                region=re.findall('itunes.apple.com/([\s\S]*?)/app',s)[0]
            except:
                region='us'
            app_url=d_app_url+'?mt=8'
            post_content=s.replace(d_app_url,'')
            post_content=post_content.replace(d_item_url,'')
            post_content=post_content.replace('?mt=8','')
            post_content=post_content.replace('&uo=4','')
            if bool(request.POST['id']):
                post_db=post()
                app_db=item.objects.get(id=int(request.POST['id']))
                post_db.item=app_db
                post_db.content=do_encode(post_content)
                post_db.author=user
                post_db.modify_time=datetime.datetime.now()
                post_db.save()
                return  render_to_response('posturl_ok.html',{'post':post_db} ,context_instance=RequestContext(request))
            else:
                
                try:
                    _d=re.findall('(itunes.apple.com[\s\S]*?/id\d{0,})',s)[0]
                    d_app_url='http://'+_d
                    d_item_url='itms://'+_d
                except:
                    return HttpResponse('notapp')
                try:
                    region=re.findall('itunes.apple.com/([\s\S]*?)/app',s)[0]
                except:
                    region='us'
                app_url=d_app_url+'?mt=8'
                post_content=s.replace(d_app_url,'')
                post_content=post_content.replace(d_item_url,'')
                post_content=post_content.replace('?mt=8','')
                post_content=post_content.replace('&uo=4','')
            #post_content=post_content.replace(d_app_url,'')
        
#----------------get app detail-------------------------------------------------
                try:
                    data=get_dict(re.findall('id(\d{0,})',s)[0],region)
                    data['trackName']
                except:
                    return HttpResponse('cantfind')          
#------------------------------end get app detail--------------------------------------
                a=item.objects.filter(trackId=data['trackId'])
#-------------------------------------------------------------------------        
                b=a.filter(Region=region)  
                time_Distance=datetime.datetime.now()-datetime.timedelta(0,48)
                import urllib2,os
                import settings
                try:
                    post_db=post()
                    try:
                        _db=item.objects.filter(trackId=int(re.findall('id(\d{0,})',s)[0])).filter(Region=region).all()[0]
                        app_db=_db.update()
                        post_db.item=app_db
                    except:
                        app_db=save2item(data,region)
                        post_db.item=app_db
                    post_db.author=user
                    post_db.content=do_encode(post_content)
                    post_db.modify_time=datetime.datetime.now()
                    post_db.save()
                    return  render_to_response('posturl_ok.html',{'post':post_db} ,context_instance=RequestContext(request))
                except:
                    return HttpResponse('False')
        '''    if not(a.exists() and b.exists()):
            #某个app可以被分享 ：1此appid不存在 2：此appid存在但是地区不一样
                import urllib2,os
                import settings
                #boob_image=urllib2.urlopen(data['url_img_app']).read()
                #file_img=open(os.path.join(settings.base_dir,'media',do_encode(data['name_app'])+'.jpg'),'wb')
                #file_img.write(boob_image)
                #file_img.close()
                post_db=post()
                app_db=save2item(data,region)
                #print 'save item ok'
                post_db.item=app_db
                #print str(user.email)
                post_db.author=user
                #print str(user.email)
                post_db.content=do_encode(post_content)
                post_db.modify_time=datetime.datetime.now()
                #print '-----------------'
                post_db.save()
                #print 'save post ok '
                return  render_to_response('posturl_ok.html',{'post':post_db} ,context_instance=RequestContext(request))
            elif  a.exists() and not(b.exists())  :
                if a[0].update_time > time_Distance:
                    # a 存在 b不存在
                    post_db=post()
                    app_db=a
                    post_db.item=app_db
                    #print str(user.email)
                    post_db.author=user
                    #print str(user.email)
                    post_db.content=do_encode(post_content)
                    post_db.modify_time=datetime.datetime.now()
                    #print '-----------------'
                    post_db.save()
                    return  render_to_response('posturl_ok.html',{'post':post_db} ,context_instance=RequestContext(request))
                else:
                    return HttpResponse('False')
            elif  a.exists() and b.exists(): 
                if b[0].update_time > time_Distance : 
                    # a 存在 b不存在
                    post_db=post()
                    app_db=b.update()
                    post_db.item=app_db
                    #print str(user.email)
                    post_db.author=user
                    #print str(user.email)
                    post_db.content=do_encode(post_content)
                    post_db.modify_time=datetime.datetime.now()
                    #print '-----------------'
                    post_db.save()
                    return  render_to_response('posturl_ok.html',{'post':post_db} ,context_instance=RequestContext(request))
                else:
                    return HttpResponse('False')
            else:  
                return HttpResponse('False')'''
    else:
        pass


@login_required    
def view_post_rating(request,flag,item_id):
    #user=request.user
    user=request.user
    if request.method == 'POST' or request.method == 'GET':
        if id and flag:
            try:
                item_db=item.objects.all().filter(id=int(item_id)).filter(Region=do_encode(region)).get()
                if Rating().check_user(user.id, item_id):
                    rate=Rating()
                    #rate.app_id=int(id)
                    if do_encode(flag) == do_encode('good'):
                        rate.rating=1
                    elif do_encode(flag) == do_encode('bad'):
                        rate.rating=-1
                    else:
                        rate.rating=0
                    rate.item=item_db
                    rate.author=user
                    rate.save()
                    newrating=Rating().get_rate(item_id)
                    return HttpResponse(newrating,'application/javascript')
                else:
                    rate=Rating.objects.filter(author=user.id).filter(item=item_db).get()
                    #rate.app_id=int(post_id)
                    if do_encode(flag) == do_encode('good'):
                        rate.rating=1
                    elif do_encode(flag) == do_encode('bad'):
                        rate.rating=-1
                    else:
                        rate.rating=0
                    rate.item=item_db
                    rate.author=user
                    rate.save()
                    newrating=Rating().get_rate(item_id)
                    return HttpResponse(newrating,'application/javascript')
            except:
                import json
                return HttpResponse(json.dumps({'good':u'暂无数据','bad':u'暂无数据'}))
        else:
            return HttpResponseRedirect('/account/login')



@login_required
def view_post_rating_new(request,flag,item_id):
    '''do the rate data use bsddb and mysql'''
    from rating import get_rating  as zu_get_rating
    from rating import check_user_vote as zu_check_user_vote
    from rating import up_vote as zu_up_vote
    from rating import down_vote as zu_down_vote
    from django.utils import simplejson as json
    #强制转换称赞整数形式
    itemid=int(item_id)
    user_id=int(request.user.id)
    if request.method == 'POST' or request.method == 'GET':
        if id and flag:
            #try:
                #此处的item获取根据item id 故而region这个可以去掉
                item_db=item.objects.all().filter(id=itemid).get()
                user_check_status=zu_check_user_vote(user_id,itemid)
                if Rating().check_user(user_id, itemid) and user_check_status  == 0:
                    #用户没有对此app做过评价 没有顶过 也没有踩过
                    #为保证数据一致性所以用and判断 两边都可以评价时才进行数据更新
                    #--------------------更新rating里面的数据---------------------------
                    rate=Rating()
                    if do_encode(flag) == do_encode('good'):
                        rate.rating=1
                    elif do_encode(flag) == do_encode('bad'):
                        rate.rating=-1
                    else:
                        rate.rating=0
                    rate.item=item_db
                    rate.author=User.objects.get(id=user_id)
                    rate.save()
                    #-----------------------------------------------------------------
                    #---------------更新BSDDB数据库的数据-------------------------------------
                    if do_encode(flag) == do_encode('good'):
                        zu_up_vote(user_id,itemid,status=user_check_status)
                    elif do_encode(flag) == do_encode('bad'):
                        zu_down_vote(user_id,itemid,status=user_check_status)
                    else:
                        pass
                    #--------------------------------------------------------------------
                    #---------------------获取好评差评数据----------------------------------
                    #newrating=Rating().get_rate(itemid)#从mysql数据库中获取数据
                    good,bad=zu_get_rating(itemid)
                    newrating=json.dumps({'good':good,'bad':bad})
                    #--------------------------------------------------------------------
                    return HttpResponse(newrating,'application/javascript')
                elif not(Rating().check_user(user_id, itemid)) and user_check_status:
                #else:
                    #用户评价过某个app的情况 保证数据一致性这里的情况是既要rating和bsd里面都评价过的情况
                    #-----------------更新rating里面的数据---------------------------------
                    rate=Rating.objects.all().filter(author=user_id).filter(item=itemid).get()
                    if do_encode(flag) == do_encode('good'):
                        rate.rating=1
                    elif do_encode(flag) == do_encode('bad'):
                        rate.rating=-1
                    else:
                        rate.rating=0
                    rate.item=item_db
                    rate.author=User.objects.get(id=user_id)
                    rate.save()
                    #--------------------------------------------------------------------
                    #----------------更新BDDDB数据库的数据------------------------------------
                    if do_encode(flag) == do_encode('good'):
                        zu_up_vote(user_id, itemid, status=user_check_status)
                    elif do_encode(flag) == do_encode('bad'):
                        zu_down_vote(user_id, itemid, status=user_check_status)
                    else:
                        pass
                    #---------------------获取好评差评数据----------------------------------
                    #newrating=Rating().get_rate(itemid)#从mysql数据库中获取数据
                #------------------------------------------------------------
                    good,bad=zu_get_rating(itemid)
                    newrating=json.dumps({'good':good,'bad':bad})
                #--------------------------------------------------------------
                    #--------------------------------------------------------------------
                    return HttpResponse(newrating,'application/javascript')
                else:
                    newrating=json.dumps({'good':0,'bad':0})
                    return HttpResponse(newrating,'application/javascript')
            #except:
                #import json
                #return HttpResponse(json.dumps({'good':u'暂无数据','bad':u'暂无数据'}))
        else:
            return HttpResponseRedirect('/account/login')

    
@login_required    
def view_new_app(request):
    #user=request.user
    user=request.user
    if request.method == 'GET':
        new_app=item.objects.all().order_by('-post_time')[:12]
        return  render_to_response('topapp.html',{'user':user,'appdata':new_app})
    else:
        HttpResponse('please use GET method')
        
@login_required
def view_user_home(request):
    #user=request.user
    user=request.user
    if request.method == 'GET':
        new_app=item.objects.all().order_by('-post_time')[:12]
        return  render_to_response('home.html',{'user':user,
                                                   'appdata':new_app,
                                                   'index_page':'current',})
    else:
        HttpResponse('please use GET method')



#@login_required
#def view_myshare(request,author_id):
#    #按用户id查看用户分享的app情况 不需要用户登录
#    from django.http import Http404
#    try:
#        try:
#            user=User.objects.get(id=int(author_id))
#           myshare=user.post_set.get_query_set().order_by('-post_time')[:10]
#        except:
#            myshare=[]
#        if request.method == 'GET':
#            return  render_to_response('home.html',{'user':user,
#                                                   'appdata':myshare,
#                                                   'title':do_encode(user.nick_name),
#                                                   'user_page':'current',})
#    except:
#        raise Http404


#@cache_control(must_revalidate=True, max_age=60*60)
#@cache_page(60*3)
@login_required    
def view_rebang(request):
    from work import cut_page
    #user=request.user
    user=request.user
    if request.method == 'GET':
        rebang=post().getrebang_new()[:50]
        #from work import cache_top_page
        #rebang=cache_top_page()
        #rebang.reverse()
        cut=cut_page(rebang,10,page=request.GET.get('page'),start=request.GET.get('start'))
        show_page=cut.cut_page()
        rebang=rebang[cut.li_slice[0]:cut.li_slice[1]]
        return  render_to_response('topapp.html',{'user':user,
                                                     'appdata':rebang,
                                                     'title':u'热榜',
                                                     'top_page':'current',
                                                     'show_page':show_page,
                                                     })
    else :
        HttpResponse('please use GET method')  
 
def view_cron(request): 
    rebang=post().getrebang()
    return HttpResponse('update to cache ok %s' %str(rebang)) 
        
#@cache_control(must_revalidate=True, max_age=3600)
#@login_required       
def view_index(request):
    from work import cut_page
    #user=request.user
    user=request.user
    if request.method == 'GET': 
        if user.is_authenticated():
            data=post.objects.all().order_by('-post_time') 
            #rebang=apps().getrebang()[:12]
            cut=cut_page(data,10,page=request.GET.get('page'),start=request.GET.get('start'))
            show_page=cut.cut_page()
            data=data[cut.li_slice[0]:cut.li_slice[1]]
            return render_to_response('home.html',{'user':user,
                                                      'appdata':data,
                                                      'title':u'果味',
                                                      'index_page':'current',
                                                      'show_page':show_page,})
        else:
            return render_to_response('index_early.html',{'user':user,
                                                      'title':u'果味',
                                                      'index_page':'current',
                                                                })
            
            
    else :
        HttpResponse('please use GET method') 
        
        
        
@login_required
def view_item_detail(request,item_id):
    from work import cut_page
    #try:
    data=item.objects.all().get(id=int(item_id))        
    #except:
    ##     raise Http404
    post_set=data.post_set.all()
    cut=cut_page(post_set,4,page=request.GET.get('page'),start=request.GET.get('start'))
    show_page=cut.cut_page()
    post_set=post_set[cut.li_slice[0]:cut.li_slice[1]]
    return render_to_response('appdetail_5.html',{    'title':''.join([u'果味','--',data.name]),
                                                      'index_page':'current',
                                                      'data':data,
                                                      'post_set':post_set,
                                                    'show_page':show_page, 
                                                                }, context_instance=RequestContext(request))
    
@login_required        
def view_app_detail(request,post_id):
    #user=request.user
    from work import cut_page
    user=request.user
    data=post.objects.filter(id=post_id).get()
    if request.method == 'GET':
        
        comm_set=data.comments_set.all().order_by('post_time')
        cut=cut_page(comm_set,20,page=request.GET.get('page'),start=request.GET.get('start'))
        show_page=cut.cut_page()
        comm=comm_set[cut.li_slice[0]:cut.li_slice[1]]
        if request.GET.get('page') == None:page=1
        else:page=request.GET.get('page')
    #-------------------------------------------
#    if request.method == 'GET':
#        #用于处理get参数， key为start 和 page的参数转换成page坐标 
#        try:
#            try:
#                #start参数用于直接开始于第几条评论定位于第几个20元素的组列
#                start=int(request.GET['start'])/20*20
#            except:
#                try:
#                    #page参数用于处理开始于第几页
#                    start=(int(request.GET['page'])-1)*20
#                except:
#                    start=0
#        except:
#            start=0
#        set_all=data.comments_set.all().order_by('post_time')
#        comm=set_all[start:start+20]
#        from django.core.paginator import Paginator
#        paginator =Paginator(set_all, 20)
#        li_page=paginator.page_range
#        num_currect_page=start/20+1  #当前页码比如第12页
#        prev_page=False
#        prev_group=False
#        next_page=False
#        next_group=False
#        if num_currect_page/10>=1:
#            li_show=li_page[(num_currect_page-1)/10*10:(num_currect_page-1)/10*10+10]
#            #判断是否显示上一组和上一页
#            if num_currect_page!=1:
#                prev_page=num_currect_page-1
#            if num_currect_page/10 >=1:
#                prev_group=num_currect_page/10*10-9
#            #判断是否有显示下一页和下一组
#            if num_currect_page%10 != 0 and num_currect_page<paginator.num_pages:
#                next_page=num_currect_page+1
#            if paginator.num_pages/10+1>(num_currect_page-1)/10:
#                next_group=(num_currect_page-1)/10*10+11
#        else:
#            li_show=li_page[:10]
#            if num_currect_page!=1:
#                prev_page=num_currect_page-1
#            if num_currect_page%10 != 0 and num_currect_page<paginator.num_pages:
#                next_page=num_currect_page+1
#            if paginator.num_pages/10+1>(num_currect_page-1)/10:
#                next_group=(num_currect_page-1)/10*10+11
#---------------------------------------------------------------------------------
        return render_to_response('post.html',{'user':user,
                                                        'post':data,
                                                        'comm':comm,
                                                        'page':page,
                                                        'show_page':show_page,
                                                        'title':data.item.name
                                                        })
    if request.method == 'POST':
        comm=Comments()
        comm.author=user
        comm.post=data
        comm.content=do_encode(request.POST['content'])
        comm.save()
        start=len(data.comments_set.all())
        return HttpResponseRedirect('/post/%s/?start=%s' %(post_id,start))
    
@login_required
def view_delete_comment(request,post_id,comment_id,page):
    user=request.user
    data=post.objects.filter(id=int(post_id)).filter(author=int(user.id))
    #comm=Comments.objects.filter(post=int(post_id)).filter(id=int(comment_id))
    if data.exists():
        #用户是该post的作者
        comm=Comments.objects.get(id=int(comment_id))
        comm.delete()
    else:
        if Comments.objects.filter(author=user.id).filter(id=int(comment_id)).exists():
            #该用户是该comm的作者可以删除
            comm=Comments.objects.filter(author=user.id).filter(id=int(comment_id)).get()
            comm.delete()
        else :
            pass
    return HttpResponseRedirect(''.join(['/post/',str(post_id),'/','?page=',str(page)]))

#@login_required
def view_myshare(request,author_id):
    from work import cut_page
    user=request.user
    try:
        user_other=User.objects.get(id=int(author_id))
    except:
        raise Http404
    peopleshare=User.objects.get(id=int(author_id))
    data=peopleshare.post_set.all().order_by('-post_time')
    #apps_db=Rating.objects.filter(author=int(author_id))
    #app_like=apps_db.filter(rating=1).order_by('-post_time')
    #app_unlike=apps_db.filter(rating=-1).order_by('-post_time')
    a=Rating()
    app_like=a.get_user_likeapplist(int(author_id)) 
    app_unlike=a.get_user_hateapplist(int(author_id))
    num_like=len(app_like)
    num_unlike=len(app_unlike)
    app_like=app_like[:12]
    app_unlike=app_unlike[:12]
    cut=cut_page(data,10,page=request.GET.get('page'),start=request.GET.get('start'))
    show_page=cut.cut_page()
    data=data[cut.li_slice[0]:cut.li_slice[1]]
    return render_to_response('profile.html',{          'user':user,
                                                      'appdata':data,
                                                      'title':''.join([user_other.userprofile_set.all()[0].nick_name,u'的果味']),
                                                      'user_other':user_other,
                                                      'num_like':num_like,
                                                      'app_like':app_like,
                                                      'app_unlike':app_unlike,
                                                      'num_unlike':num_unlike,
                                                      'user_page':'current',
                                                      'show_page':show_page,})
 
def view_redirect(request):
        return HttpResponseRedirect('/')
    
    
@login_required
def view_app_like(request,item_id,preference):
    #user who like this app
    from work import cut_page
    user=request.user
    item_db=item.objects.get(id=int(item_id))
    print preference
    if len(preference) == len('like'):
        print 'if'
        data=item_db.get_people_like
        title=u'''喜欢"%s"的成员''' %(item_db.name)
        pf=u'喜欢'
        pd=u'不太喜欢'
        lk='unlike'
        show_page=cut_page(data,70,page=request.GET.get('page'),start=request.GET.get('start')).cut_page()
    elif len(preference) == len('unlike'):
        print 'if unlike'
        data=item_db.get_people_hate
        title=u'''不喜欢"%s"的成员''' %(item_db.name)
        pf=u'不太喜欢'
        pd=u'喜欢'
        lk='like'
        show_page=cut_page(data,70,page=request.GET.get('page'),start=request.GET.get('start')).cut_page()
    else:
        print 'else'
        data=[]
    return render_to_response('userlist.html',{  'user':user,
                                                      'item':item_db,
                                                      'pf':pf,
                                                      'lenpf':str(len(pf)),
                                                      'pd':pd,
                                                      'lk':lk,
                                                      'data':data,
                                                      'show_page':show_page,                                                      
                                                      'post_id':item_id,
                                                      'title':title,
                                                      })
        
    
@login_required
def view_user_likeapp(request,preference,author_id):    
    #user's like app list
    user=request.user
    try:
        user_other=User.objects.get(id=int(author_id))
    except:
        raise Http404
    from work import cut_page
    apps_db=Rating.objects.filter(author=int(author_id))
    app_like=apps_db.filter(rating=1)
    app_unlike=apps_db.filter(rating=-1)
    num_like=len(app_like)
    num_unlike=len(app_unlike)
    if request.method == 'GET':
        if preference == 'like':
            title=u'''%s喜欢的app''' %(user_other.get_profile().nick_name)
            pf=u'喜欢'
            pfq='like'
            apps=app_like
            num_top=num_like
            num_foot=num_unlike
            lk='unlike'
            pd=u'不喜欢'
            cut=cut_page(apps,70,page=request.GET.get('page'),start=request.GET.get('start'))
            show_page=cut.cut_page()
            sli=cut.li_slice
            apps=app_like[sli[0]:sli[1]]
        elif preference == 'unlike':
            title=u'''%s不喜欢的app''' %(user_other.get_profile().nick_name)
            apps=app_unlike
            pf=u'不喜欢'
            pfq='unlike'
            num_top=num_unlike
            num_foot=num_like
            lk='like'
            pd=u'喜欢'
            cut=cut_page(apps,70,page=request.GET.get('page'),start=request.GET.get('start'))
            show_page=cut.cut_page()
            sli=cut.li_slice
            apps=app_unlike[sli[0]:sli[1]]
        return render_to_response('applist.html',{    'user':user,
                                                            'user_other':user_other,
                                                            'apps':apps,
                                                            'pf':pf,
                                                            'pd':pd,
                                                            'pfq':pfq,
                                                            'lenpf':str(len(pf)),
                                                            'lk':lk,
                                                            'num_top':num_top,
                                                            'num_foot':num_foot,
                                                            'show_page':show_page, 
                                                            'title':title,
                                                            })  
        
        
def view_checklike(request,flag,item_id,author_id):   
    a=Rating.objects.filter(author=user_id).filter(item=item_id)
    if a.exists():
        #数据库中可以查出数据，用户对该app有评价过
        if a.rating == 1  and flag == 'good':
            return HttpResponse('bad_red')
        elif a.rating == -1 and flag == 'bad':
            return HttpResponse('good_black')
    else:
        #用户对该app没有评价
        if flag == 'good':
            return HttpResponse('bad')
        elif flag == 'bad':
            return HttpResponse('good')
        
def updatedb():
    start=datetime.datetime.now()
    end=end = start - datetime.timedelta(hours=30, minutes=0, seconds=0) 
    a=item.objects.filter(update_time__lt(end))
    import thread,time
    for i in a:
        thread.start_new_thread(i.update())
    return HttpResponse('update %s items ok' %(len(a)))
