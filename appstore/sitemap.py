# -*- coding:utf-8 -*-
from django.contrib.sitemaps import Sitemap
from appstore.models import post

class postSitemap(Sitemap):
    changefreq = "hourly"
    priority = 0.8
    location='/'

    def items(self):
        return post.objects.all().order_by('-post_time')

    def lastmod(self, obj):
        return obj.modify_time


sitemaps = {
            'subpage':postSitemap
}


    
from django.conf.urls.defaults import *
from django.contrib.sitemaps import FlatPageSitemap, GenericSitemap


urlpatterns = patterns('',
    # some generic view using info_dict
    # ...

    # the sitemap
    (r'^$', 'django.contrib.sitemaps.views.sitemap', {'sitemaps': sitemaps})
)