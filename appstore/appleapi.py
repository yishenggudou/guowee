# -*- coding:utf-8 -*-

import urllib2


def price_format(_price,cu): 
    if float(_price) == 0:
        return u'免费'
    else:
        from currency import dic_currency
        s=dic_currency.get(cu)
        if s[1]=='L':
            price=lambda x :str('%d' %(x)) if x.is_integer() else str('%.2f' %(x))
            return  ''.join([s[0],str(price(_price))]) 
        elif   s[1]=='R':
            price=lambda x :str('%d' %(x)) if x.is_integer() else str('%.2f' %(x)),self.price
            return  ''.join([str(price(_price)),s[0]]) 
def get_dict(id,country):
    url='http://ax.itunes.apple.com/WebObjects/MZStoreServices.woa/wa/wsLookup?id=%s&country=%s'
    print country
    import json
    try:
        d=json.loads(urllib2.urlopen(str(url %(str(id),'cn'))).read())['results'][0]
    except:
        try:
            d=json.loads(urllib2.urlopen(str(url %(str(id),'us'))).read())['results'][0]
        except:
            d=json.loads(urllib2.urlopen(str(url %(str(id),country))).read())['results'][0]
            
    d['_price']=price_format(float(d.get('price')),d.get('currency'))
    return d

dict_gener={u'7018': u'\u5c0f\u6e38\u620f', u'7019': u'\u6587\u5b57\u6e38\u620f', u'7016': u'\u4f53\u80b2', u'7017': u'\u7b56\u7565\u6e38\u620f', u'7014': u'\u89d2\u8272\u626e\u6f14\u6e38\u620f', u'7015': u'\u6a21\u62df\u6e38\u620f', u'7012': u'\u667a\u529b\u6e38\u620f', u'7013': u'\u8d5b\u8f66\u6e38\u620f', u'7010': u'\u513f\u7ae5\u6e38\u620f', u'7011': u'\u97f3\u4e50', u'6006': u'\u53c2\u8003', u'6007': u'\u6548\u7387', u'6004': u'\u4f53\u80b2', u'6005': u'\u793e\u4ea4', u'6002': u'\u5de5\u5177', u'6003': u'\u65c5\u884c', u'6000': u'\u5546\u4e1a', u'6001': u'\u5929\u6c14', u'6008': u'\u6444\u5f71', u'6009': u'\u65b0\u95fb', u'6020': u'\u533b\u7597', u'7009': u'\u5bb6\u5ead\u6e38\u620f', u'7008': u'\u6559\u80b2\u6e38\u620f', u'7001': u'\u52a8\u4f5c\u6e38\u620f', u'7003': u'\u8857\u673a\u6e38\u620f', u'7002': u'\u63a2\u9669\u6e38\u620f', u'7005': u'\u6251\u514b\u724c\u6e38\u620f', u'7004': u'\u684c\u9762\u6e38\u620f', u'7007': u'\u9ab0\u5b50\u6e38\u620f', u'7006': u'\u8d4c\u573a\u6e38\u620f', u'6011': u'\u97f3\u4e50', u'6010': u'\u5bfc\u822a', u'6013': u'\u5065\u5eb7', u'6012': u'\u751f\u6d3b', u'6015': u'\u8d22\u52a1', u'6014': u'\u6e38\u620f', u'6017': u'\u6559\u80b2', u'6016': u'\u5a31\u4e50', u'6018': u'\u4e66\u7c4d'}

from models import item
def save2item(args_set,Region,obj=None):
        if obj==None:
            item_db=item()  
        else:
            item_db=obj
        item_db.Region      =Region
        item_db.version      =args_set.get('version')
        item_db.kind         =args_set.get('kind')
        item_db.artistId     =int(args_set.get('artistId'))
        item_db.artistName   =args_set.get('artistName')
        item_db.price        =float(args_set.get('price'))
        item_db.description  =args_set.get('description')
        item_db.genreIds     ='+'.join(args_set.get('genreIds'))
        item_db.releaseDate  =args_set.get('releaseDate')
        item_db.sellerName   =args_set.get('sellerName')         
        item_db.currency     =args_set.get('currency')    #"USD" 
        item_db.trackId      =int(args_set.get('trackId'))
        item_db.trackName    =args_set.get('trackName')          #"\u91d1\u9f9f\u4fa0\u58eb"
        item_db.genres       ='+'.join(args_set.get('genres'))          # ["Games", "Action", "Adventure"]
        item_db.releaseNotes = args_set.get('releaseNotes')       #  ""
        item_db.primaryGenreName = args_set.get('primaryGenreName')        #"Games"
        item_db.primaryGenreId   =int(args_set.get('primaryGenreId') )        #6014
        item_db.isGameCenterEnabled= args_set.get('isGameCenterEnabled')
        try: #true
            item_db.supportedDevices ='+'.join(args_set.get('supportedDevices'))
        except:
            item_db.supportedDevices=''
        item_db.wrapperType      =args_set.get('wrapperType')                #,            "software"
        item_db.artworkUrl60     =args_set.get('artworkUrl60') #"http,//a4.mzstatic.com/us/r1000/000/Purple/41/d2/1c/mzi.vwbugnuq.png"
        item_db.artworkUrl100    =args_set.get('artworkUrl100')   # "http,//a5.mzstatic.com/us/r1000/028/Purple/6c/38/f5/mzi.nsxmbxvl.png"
        item_db.artistViewUrl    =args_set.get('artistViewUrl')      #"http,//itunes.apple.com/us/artist/vng-corporation/id398453277?uo=4"
        item_db.contentAdvisoryRating    =args_set.get('contentAdvisoryRating')   #"4+"
        item_db.trackCensoredName        =args_set.get('trackCensoredName')  #"\u91d1\u9f9f\u4fa0\u58eb"
        item_db.trackViewUrl             =args_set.get('trackViewUrl')        # "http,//itunes.apple.com/us/app/id411878924?mt=8&uo=4"
        item_db.languageCodesISO2A       ='+'.join(args_set.get('languageCodesISO2A'))      # ["EN"] 
        item_db.fileSizeBytes            =int(args_set.get('fileSizeBytes'))    #"25679692"
        item_db.screenshotUrls  ='|'.join(args_set.get('screenshotUrls'))     #[
        item_db.ipadScreenshotUrls='|'.join(args_set.get('ipadScreenshotUrls'))      #  []
        item_db.sellerUrl       =str(args_set.get('sellerUrl'))         # null
        item_db.averageUserRatingForCurrentVersion =args_set.get('averageUserRatingForCurrentVersion')        #    ,5.0
        item_db.userRatingCountForCurrentVersion=args_set.get('userRatingCountForCurrentVersion')        #  2
        item_db.artworkUrl512   =args_set.get('artworkUrl512')    # http,//a5.mzstatic.com/us/r1000/028/Purple/6c/38/f5/mzi.nsxmbxvl.png"
        item_db.trackContentRating=args_set.get('trackContentRating')      # "4+"
        item_db.averageUserRating=args_set.get('averageUserRating')      #  3.5
        item_db.userRatingCount=args_set.get('userRatingCount')
        
        
        
        
        
        
        '''Region      =Region
        version      =args_set.get('version')
        kind         =args_set.get('kind')
        artistId     =int(args_set.get('artistId'))
        artistName   =args_set.get('artistName')
        price        =float(args_set.get('price'))
        description  =args_set.get('description')
        genreIds     ='+'.join(args_set.get('genreIds'))
        releaseDate  =args_set.get('releaseDate')
        sellerName   =args_set.get('sellerName')         
        currency     =args_set.get('currency')    #"USD" 
        trackId      =int(args_set.get('trackId'))
        trackName    =args_set.get('trackName')          #"\u91d1\u9f9f\u4fa0\u58eb"
        genres       ='+'.join(args_set.get('genres'))          # ["Games", "Action", "Adventure"]
        releaseNotes = args_set.get('releaseNotes')       #  ""
        primaryGenreName = args_set.get('primaryGenreName')        #"Games"
        primaryGenreId   =int(args_set.get('primaryGenreId') )        #6014
        isGameCenterEnabled= args_set.get('isGameCenterEnabled')
        try: #true
            supportedDevices ='+'.join(args_set.get('supportedDevices'))
        except:
            supportedDevices=''
        wrapperType      =args_set.get('wrapperType')                #,            "software"
        artworkUrl60     =args_set.get('artworkUrl60') #"http,//a4.mzstatic.com/us/r1000/000/Purple/41/d2/1c/mzi.vwbugnuq.png"
        artworkUrl100    =args_set.get('artworkUrl100')   # "http,//a5.mzstatic.com/us/r1000/028/Purple/6c/38/f5/mzi.nsxmbxvl.png"
        artistViewUrl    =args_set.get('artistViewUrl')      #"http,//itunes.apple.com/us/artist/vng-corporation/id398453277?uo=4"
        contentAdvisoryRating    =args_set.get('contentAdvisoryRating')   #"4+"
        trackCensoredName        =args_set.get('trackCensoredName')  #"\u91d1\u9f9f\u4fa0\u58eb"
        trackViewUrl             =args_set.get('trackViewUrl')        # "http,//itunes.apple.com/us/app/id411878924?mt=8&uo=4"
        languageCodesISO2A       ='+'.join(args_set.get('languageCodesISO2A'))      # ["EN"] 
        fileSizeBytes            =int(args_set.get('fileSizeBytes'))    #"25679692"
        screenshotUrls  ='|'.join(args_set.get('screenshotUrls'))     #[
        ipadScreenshotUrls='|'.join(args_set.get('ipadScreenshotUrls'))      #  []
        sellerUrl       =str(args_set.get('sellerUrl'))         # null
        averageUserRatingForCurrentVersion =args_set.get('averageUserRatingForCurrentVersion')        #    ,5.0
        userRatingCountForCurrentVersion=args_set.get('userRatingCountForCurrentVersion')        #  2
        artworkUrl512   =args_set.get('artworkUrl512')    # http,//a5.mzstatic.com/us/r1000/028/Purple/6c/38/f5/mzi.nsxmbxvl.png"
        trackContentRating=args_set.get('trackContentRating')      # "4+"
        averageUserRating=args_set.get('averageUserRating')      #  3.5
        userRatingCount=args_set.get('userRatingCount')
        
        item_db=item(Region,version,kind,artistId,
                 artistName,price,description,genreIds,
                 releaseDate ,sellerName ,currency,trackId,
                 trackName,genres,releaseNotes,primaryGenreName,
                 primaryGenreId,isGameCenterEnabled,supportedDevices ,
                 wrapperType,artworkUrl60,artworkUrl100,artistViewUrl,
                 contentAdvisoryRating,trackCensoredName,trackViewUrl ,
                 languageCodesISO2A,fileSizeBytes,screenshotUrls,
                 ipadScreenshotUrls,sellerUrl ,averageUserRatingForCurrentVersion,
                 userRatingCountForCurrentVersion,artworkUrl512,trackContentRating,
                 averageUserRating,userRatingCount,)'''
        
        
        
        
        item_db.save()
        return  item_db