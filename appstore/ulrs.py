# -*- coding:utf-8 -*-
from django.conf.urls.defaults import *

urlpatterns = patterns('guowee.appstore.views',
     (r'^posturl/','view_post_appurl'),
     (r'^top','view_rebang'),
     (r'^new','view_new_app'),
     (r'^user/\S{0,30}','view_user_home'),
     (r'^post/(?P<post_id>\d{0,})','view_app_detail'),
     (r'^myshare','view_myshare'),
     (r'^rate/(?P<flag>\S{0,4})/(?P<appid>\d*?)/(?P<region>.*?)/','view_post_rating'),
     (r'^accounts/profile/$','view_login'),
     )