# Create your views here.
from django.contrib.auth.models import User 
from django.contrib.auth.decorators import login_required
from django.http import Http404, HttpResponse, HttpResponseRedirect,HttpResponseServerError 
from django.shortcuts import render_to_response
from comment.models import *
from appstore.get_apple_appstore import do_encode

def index_comment(request):
    if request.method == 'GET':
        return render_to_response('post_comment.html',
                                                {'user':request.user,
                                                'data':})
    else:
        return  HttpResponse('Please Use GET Method')

def add_newpost(request):
    user=request.user
    if request.method == 'GET':
        return render_to_response('post_comment.html',
                                                {'user':request.user,
                                                'data':})
    elif  request.method == 'POST': 
        p=post()
        p.author=user
        p.content=do_encode(request.POST['content'])
        p.save()
        return HttpResponse('some html')
    else:
        raise HttpResponseServerError
        
def post_comment(request,post_id):
    user=request.user
    if request.method == 'POST':
        comm=comment()
        comm.author=user
        comm.post=post.object.get(id=int(post_id))
        comm.content=do_encode(request.POST['content'])
        comm.save()
        start=len(data.comments_set.all())
        return HttpResponseRedirect('/comment/%s?start=%s' %(post_id,start))
        
    
