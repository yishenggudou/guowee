# --*--coding:utf-8 --*--
from django.db import models
from django.db import models
from django.contrib.auth.models import User
# Create your models here.
from appstore.get_apple_appstore import do_encode
class post(models.Model):
    id=models.AutoField('ID',primary_key=True)
    author=models.ForeignKey(User,verbose_name=u"发布者") 
    content=models.TextField()
    goodrate=models.IntegerField(null=True)
    badrate=models.IntegerField(null=True)
    post_time=models.DateTimeField(auto_now_add=True)
    
    def __str__(self):
        return do_encode(self.id)
    def __unicode__(self):
        return do_encode(self.id)
    
class comment(models.Model):
    id=models.AutoField('ID',primary_key=True)
    author=models.ForeignKey(User,verbose_name=u"发布者") 
    post=models.ForeignKey(post,verbose_name=u"评论")
    content=models.TextField()
    goodrate=models.IntegerField(null=True)
    badrate=models.IntegerField(null=True)
    post_time=models.DateTimeField(auto_now_add=True)
    
    def __str__(self):
        return do_encode(self.id)
    def __unicode__(self):
        return do_encode(self.id)
