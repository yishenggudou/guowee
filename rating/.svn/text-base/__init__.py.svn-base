#! /usr/bin/python
# -*- coding: utf-8 -*-

import os
import struct
from bsddb import _checkflag, _openDBEnv, db, _DBWithCursor


#这个库中以 ID 为 key , value 是两个无符号的 long 型整数, 共 8 个字节
APP_COUNT_BSDDB = os.path.join(os.path.dirname(__file__), 'app_count.bsddb')

#这个库以用户 ID 为 key, value 是两个无符号的 long 型整数, 记录用户顶和踩的总数
USER_COUNT_BSDDB = os.path.join(os.path.dirname(__file__), 'user_count.bsddb')

#保存用户对 app 的评价情况的 1 个 bsd 库, 保存了多少个 app 的信息
#4096 个应用, 需要 8192 位空间(一个应用 2 位, 表示顶或踩), 即 1K 的空间.
#按 10W 用户算, 1 个 bsd 库的大小在 100M 左右.
APP_MOUNT_PER_BSDDB = 4096

#存发评价情况 bsd 库的目录
VOTE_BSDDB_DIR = os.path.dirname(__file__)


def _get_vote_fullname(app_id):
    '获取此 app_id 对应的完整 bsd 库的路径'

    n = app_id / APP_MOUNT_PER_BSDDB
    filename = '%s-%s.bsddb' % \
               (APP_MOUNT_PER_BSDDB * n, APP_MOUNT_PER_BSDDB * (n + 1) - 1)
    return os.path.join(VOTE_BSDDB_DIR, filename)


def quopen(file, flag='c', mode=0666,
           quflags=0, cachesize=None, pgsize=None, lorder=None,
           rlen=None, delim=None, source=None, pad=None):
    '以 QUEUE 方式使用 bdb . 使用时一定要指定 rlen 参数'

    flags = _checkflag(flag, file)
    e = _openDBEnv(cachesize)
    d = db.DB(e)
    if pgsize is not None: d.set_pagesize(pgsize)
    if lorder is not None: d.set_lorder(lorder)
    d.set_flags(quflags)
    if delim is not None: d.set_re_delim(delim)
    if rlen is not None: d.set_re_len(rlen)
    if source is not None: d.set_re_source(source)
    if pad is not None: d.set_re_pad(pad)
    d.open(file, db.DB_QUEUE, flags, mode)
    return _DBWithCursor(d)


def _up_vote(app_id, value=+1):
    '顶一个 app , 顶的值如何改变由 value 参数控制'

    d = quopen(APP_COUNT_BSDDB, 'w', rlen=8)
    try:
        up, down = struct.unpack('<LL', d[app_id])
    except KeyError:
        up, down = 0, 0
    up += value
    d[app_id] = struct.pack('<LL', up, down)
    d.close()

    return up, down


def _user_count_up(user_id, value=+1):
    '用户顶的总数改变'

    d = quopen(USER_COUNT_BSDDB, 'w', rlen=8)
    try:
        up, down = struct.unpack('<LL', d[user_id])
    except KeyError:
        up, down = 0, 0
    up += value
    d[user_id] = struct.pack('<LL', up, down)
    d.close()

    return up, down


def _user_count_down(user_id, value=+1):
    '用户踩的总数改变'

    d = quopen(USER_COUNT_BSDDB, 'w', rlen=8)
    try:
        up, down = struct.unpack('<LL', d[user_id])
    except KeyError:
        up, down = 0, 0
    down += value
    d[user_id] = struct.pack('<LL', up, down)
    d.close()

    return up, down


def _down_vote(app_id, value=+1):
    '踩一个 app , 踩的值如何改变由 value 参数控制'

    d = quopen(APP_COUNT_BSDDB, 'w', rlen=8)
    try:
        up, down = struct.unpack('<LL', d[app_id])
    except KeyError:
        up, down = 0, 0
    down += value
    d[app_id] = struct.pack('<LL', up, down)
    d.close()
    return up, down


def get_rating(id):
    '获取一个 app 的顶踩情况'

    try:
        d = quopen(APP_COUNT_BSDDB, 'r', rlen=8)
    except db.DBNoSuchFileError:
        return 0, 0 

    try:
        up, down = struct.unpack('<LL', d[id])
    except KeyError:
        d.close()
        return 0, 0
    else:
        d.close()
        return up, down


def check_user_vote(user_id, app_id):
    '''获取一个用户是否 顶/踩 过指定 id 的 app
    返回值:
        0 没有顶过也没有踩过
        1 踩过
        2 顶过
    '''

    #根据 app_id 找到对应的 bsd 库文件
    n = app_id / APP_MOUNT_PER_BSDDB
    filename = _get_vote_fullname(app_id)

    #在库中的第多少个应用, 索引从 0 开始
    order = app_id - APP_MOUNT_PER_BSDDB * n

    #计算是库中的第多少个 char , 索引从 0 开始
    char_order = order / 4

    #计算是 char 中的第多少位. 共占用 2 位 , 前位表'顶', 后位表'踩'
    bit_order = order % 4 * 2

    #一共多少个字节
    l = APP_MOUNT_PER_BSDDB * 2 / 8

    try:
        d = quopen(filename, 'r', rlen=l)
    except db.DBNoSuchFileError:
        d = quopen(filename, 'w', rlen=l)
        d[user_id] = struct.pack('<%sx' % l)
        d.close()
        return 0

    try:
        data = struct.unpack('<%sc' % l, d[user_id])
        d.close()
    except KeyError:
        d.close()
        d = quopen(filename, 'w', rlen=l)
        d[user_id] = struct.pack('<%sx' % l)
        d.close()
        return 0

    n = ord(data[char_order])

    #Python中的位移操作对溢出检查有问题-_-
    #还是取字符串吧
    if n == 0:
        return 0
    else:
        return int('0' + bin(n >> (6 - bit_order))[-2:], 2)


def _user_up_mark(user_id, app_id):
    '标记用户顶过某 app '

    #根据 app_id 找到对应的 bsd 库文件
    n = app_id / APP_MOUNT_PER_BSDDB
    filename = _get_vote_fullname(app_id)

    #在库中的第多少个应用, 索引从 0 开始
    order = app_id - APP_MOUNT_PER_BSDDB * n

    #计算是库中的第多少个 char , 索引从 0 开始
    char_order = order / 4

    #计算是 char 中的第多少位. 共占用 2 位 , 前位表'顶', 后位表'踩'
    bit_order = order % 4 * 2

    #一共多少个字节
    l = APP_MOUNT_PER_BSDDB * 2 / 8

    d = quopen(filename, 'w', rlen=l)
    try:
        data = struct.unpack('<%sc' % l, d[user_id])
    except KeyError:
        data = ('\x00', ) * l

    n = ord(data[char_order])

    #要得到一个固定长度的二进制表示
    bn = bin(n)[2:]
    bn = '0' * (8 - len(bn)) + bn
    bn = '0b' + bn[0:bit_order] + '10' + bn[bit_order + 2:]

    data = list(data)
    data[char_order] = chr(int(bn, 2))
    d[user_id] = struct.pack('<%sc' % l, *data)
    d.close()
    return True


def _user_down_mark(user_id, app_id):
    '标记用户踩过某 app '

    #根据 app_id 找到对应的 bsd 库文件
    n = app_id / APP_MOUNT_PER_BSDDB
    filename = _get_vote_fullname(app_id)

    #在库中的第多少个应用, 索引从 0 开始
    order = app_id - APP_MOUNT_PER_BSDDB * n

    #计算是库中的第多少个 char , 索引从 0 开始
    char_order = order / 4

    #计算是 char 中的第多少位. 共占用 2 位 , 前位表'顶', 后位表'踩'
    bit_order = order % 4 * 2

    #一共多少个字节
    l = APP_MOUNT_PER_BSDDB * 2 / 8

    d = quopen(filename, 'w', rlen=l)
    try:
        data = struct.unpack('<%sc' % l, d[user_id])
    except KeyError:
        data = ('\x00', ) * l

    n = ord(data[char_order])

    #要得到一个固定长度的二进制表示
    bn = bin(n)[2:]
    bn = '0' * (8 - len(bn)) + bn
    bn = '0b' + bn[0:bit_order] + '01' + bn[bit_order + 2:]

    data = list(data)
    data[char_order] = chr(int(bn, 2))
    d[user_id] = struct.pack('<%sc' % l, *data)
    d.close()
    return True


def up_vote(user_id, app_id, status=None):
    '''用户顶一个 app , 要根据用户是否对这个 app 评价过而做不同的处理
    status 标示用户的状态:
        0: 没有评价过这个 app 
        1: 踩过这个 app
        2: 顶过这个 app
    '''

    if status is None:
        status = check_user_vote(user_id, app_id)

    if status == 0:
        #没有顶过
        _user_up_mark(user_id, app_id)
        up, down = _user_count_up(user_id, +1)
        return (_up_vote(app_id), (up, down))

    elif status == 1:
        #踩过
        _user_up_mark(user_id, app_id)
        _down_vote(app_id, -1)
        _user_count_down(user_id, -1)
        up, down = _user_count_up(user_id, +1)
        return (_up_vote(app_id), (up, down))

    elif status == 2:
        #已经顶过
        return (get_rating(app_id), get_user_count(user_id))


def down_vote(user_id, app_id, status=None):
    '''用户踩一个 app , 要根据用户是否对这个 app 评价过而做不同的处理
    status 标示用户的状态:
        0: 没有评价过这个 app 
        1: 踩过这个 app 
        2: 顶过这个 app 
    '''

    if status is None:
        status = check_user_vote(user_id, app_id)

    if status == 0:
        #没有踩过
        _user_down_mark(user_id, app_id)
        up, down = _user_count_down(user_id, +1)
        return (_down_vote(app_id), (up, down))

    elif status == 1:
        #已经踩过
        return (get_rating(app_id), get_user_count(user_id))

    elif status == 2:
        #顶过
        _user_down_mark(user_id, app_id)
        _up_vote(app_id, -1)
        _user_count_up(user_id, -1)
        up, down = _user_count_down(user_id, +1)
        return (_down_vote(app_id), (up, down))


def get_user_count(user_id):
    '获取用户顶和踩的总数'

    try:
        d = quopen(USER_COUNT_BSDDB, 'r', rlen=8)
    except db.DBNoSuchFileError:
        return 0, 0 

    try:
        up, down = struct.unpack('<LL', d[user_id])
    except KeyError:
        d.close()
        return 0, 0
    else:
        d.close()
        return up, down


if __name__ == '__main__':
    pass
    #print up_vote(1, 1)
    #print check_user_vote(1, 1)
    #print down_vote(1, 1)
    #print get_rating(1)
    #print get_user_count(1)