#! /usr/bin/python
# -*- coding: utf-8 -*-

'''
添加新的邀请码到备选库中
'''

import random

from django.core.management.base import BaseCommand
from invitation.models import InviteCode

class Command(BaseCommand):
    def handle(self, count, *args, **kargs):
        S1 = 'abcdefghijklmnopqrstuvwxyz'
        S2 = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'
        S3 = '0123456789'
        S = S1 + S2 + S3

        CHAR_LEN = 10

        for i in range(int(count)):
            while True:
                code = ''
                for j in range(CHAR_LEN):
                    code += random.choice(S)
                try:
                    InviteCode.objects.get(code=code)
                except InviteCode.DoesNotExist:
                    InviteCode(code=code).save()
                    break

