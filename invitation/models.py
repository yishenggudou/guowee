#! /usr/bin/python
# -*- coding: utf-8 -*-

from django.contrib.auth.models import User
from django.db import models


class InviteCode(models.Model):
    '存放备选的邀请码'

    id = models.AutoField('流水号', primary_key=True)
    code = models.CharField('邀请码', max_length=10, db_index=True)


class InviteCounter(models.Model):
    '邀请码计数'

    id = models.AutoField('流水号', primary_key=True)
    user = models.ForeignKey(User, verbose_name='用户')
    code_counter = models.IntegerField('剩余邀请码数量')
    code_max = models.IntegerField('周期更新最大邀请码数量')


class Invitation(models.Model):
    '邀请的发送情况'

    id = models.AutoField('流水号', primary_key=True)
    user = models.ForeignKey(User, verbose_name='用户')
    code = models.CharField('邀请码', max_length=10, db_index=True)
    to_email = models.EmailField('被邀请Email', db_index=True)
    flag = models.IntegerField('code是否被使用')
    send_time = models.DateTimeField('发放邀请码时间', auto_now_add=True)
