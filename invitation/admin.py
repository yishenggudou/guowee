#! /usr/bin/python
# -*- coding: utf-8 -*-

from django.contrib import admin
from models import InviteCode, InviteCounter, Invitation

class InviteCodeAdmin(admin.ModelAdmin):
    list_display = ('id', 'code')
admin.site.register(InviteCode, InviteCodeAdmin)


class InviteCounterAdmin(admin.ModelAdmin):
    list_display = ('id', 'user', 'code_counter', 'code_max')
admin.site.register(InviteCounter, InviteCounterAdmin)

class InvitationAdmin(admin.ModelAdmin):
    list_display = ('id', 'user', 'code', 'to_email', 'flag', 'send_time')
admin.site.register(Invitation, InvitationAdmin)
