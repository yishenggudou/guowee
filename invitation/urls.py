# -*- coding:utf-8 -*-

from django.conf.urls.defaults import *
from django.contrib import admin

admin.autodiscover()

from views import (send_invitation,
                   invite_register,
                  )


urlpatterns = patterns('',
     (r'^send$', send_invitation),
     (r'^invite/$', invite_register),
)
