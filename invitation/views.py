#! /usr/bin/python
# -*- coding: utf-8 -*-

import urllib
import re

from django.shortcuts import render_to_response
from django.http import HttpResponseRedirect, HttpResponse
from django.contrib.auth.decorators import login_required
from django.template import RequestContext
from django.core.mail import send_mail as django_send_mail
from django.conf import settings

from models import (InviteCode,
                    InviteCounter,
                    Invitation,)


@login_required
def send_invitation(request):
    '向指定的 email 地址发送邀请码'

    if request.method != 'POST':
        send_code_list = Invitation.objects.filter(user=request.user)
        #for code in send_code_list:
        #    params = {'email': code.to_email, 'code': code.code}
        #    url = 'http://guowee.com/invitation/invite/?' + urllib.urlencode(params)
        #    code.url = url

        try:
            counter_obj = InviteCounter.objects.get(user=request.user)
            counter = counter_obj.code_counter
        except InviteCounter.DoesNotExist:
            InviteCounter(user=request.user,
                          code_counter=1,
                          code_max=1).save()
            counter = 1


        return render_to_response('invitation.html', {'title': u'发送邀请码',
                                                       'send_code_list': send_code_list,
                                                       'counter': counter},
                                  context_instance=RequestContext(request))

    counter_obj = InviteCounter.objects.get(user=request.user)
    if counter_obj.code_counter == 0:
        waring = {
            'main': u'发送失败',
            'details': u'你的邀请码已经用完',
            'helper': u'',
            'url': '/invitation/send',
            'url_text': u'返回',
        }
        return render_to_response('mwaring.html', {'waring': waring,
                                                     'title': u'发送失败',},
                                  context_instance=RequestContext(request))
    else:
        counter_obj.code_counter -= 1
        counter_obj.save()

    to_email = request.POST['email']

    email_re = r'^[a-z0-9.+_-]+@[a-z0-9.-]{2,}\.[a-z]{2,}$'
    if re.match(email_re, to_email) is None:
        waring = {
            'main': u'发送失败',
            'details': u'邮箱地址不正确',
            'helper': u'',
            'url': '/invitation/send',
            'url_text': u'返回',
        }
        return render_to_response('waring.html', {'waring': waring,
                                                     'title': u'发送失败',},
                                  context_instance=RequestContext(request))

    try:
        code_obj = InviteCode.objects.all()[0]
    except IndexError:
        counter_obj.code_counter += 1
        counter_obj.save()
        return HttpResponse(u'<script>alert("没有备选的邀请码了, 请联系网站管理员!");history.back();</script>')

    code = code_obj.code
    code_obj.delete()

    Invitation(user=request.user,
               code=code,
               to_email=to_email,
               flag=0).save()

    params = {'email': to_email, 'code': code}
    url = 'http://guowee.com/invitation/invite/?' + urllib.urlencode(params)
    emailcontent=settings.EMAIL_CONTEXT_INVITE %(request.user.userprofile_set.all()[0].nick_name,url)
    title=u'%s邀请你加入果味，马上来吧！' %(request.user.userprofile_set.all()[0].nick_name)
    django_send_mail(title, emailcontent,
                     settings.DEFAULT_FROM_EMAIL, [to_email])

    waring = {
        'main': u'发送成功',
        'details': u'',
        'helper': u'',
        'url': '/invitation/send',
        'url_text': u'返回',
    }
    return render_to_response('waring.html', {'waring': waring,
                                                 'title': u'发送成功',},
                                  context_instance=RequestContext(request))


def invite_register(request):
    '被邀请过来注册的'

    params = {'code': request.GET.get('code', ''),
              'email': request.GET.get('email', '')}
    return HttpResponseRedirect('/account/register?' + urllib.urlencode(params))
